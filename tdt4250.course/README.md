<h2>The main classes:</h2>
<ul>
<li><b>Course:</b> The main object of a course. Containing data which are persistent. Contains credit reductions and instances for each semester</li>
<li><b>CourseInstance:</b> Containing data for a semester of the course such as timetables, evaluations forms, info and staff</li>
<li><b>Timetable:</b> A object which represent a typical event in the timetable. Contains day, from/to, weeks, type, room and for whom the event is planned for</li>
<li><b>ContactInfo:</b> A object containing who is responsible for the course. Coordinator, lecturers and department</li>
<li><b>CourseWork:</b> A object containing how many hours the course have for labs and lectures</li>
<li><b>CreditReduction:</b> A object containing data of a credit reduction, such as course code, reduction and time form and to</li>
<li><b>EvaluationForm:</b> Object containing data of a evaluation form. Which form and weight, but also duration and aids for an examination</li>
<li><b>ExaminationAid:</b> Object containing data of an examination aid. What is allowed on the examination and link to website</li>
<li><b>Study:</b> Object containing data of a study. Shot name and full name and also link to website</li>
<li><b>Room:</b> Object containing data of a room. Name of the room and a link to MazeMap</li>
<li><b>Staff:</b> Object containing data of a staff. Coordinator, lecturers and other who are involved</li>
<li><b>Department:</b> Object containing data of a department. Name, website and phone number of the department</li>
<li><b>Person:</b> Object containing data of a person. Firstname, lastname and website</li>
<li><b>Time:</b> A object containing time as hour and minute. This class also includes a method which returns the difference between it self and another time instance</li>
</ul>

<h2>Enums</h2>
<ul>
<li><b>EvaluationType:</b> The types of evaluation forms</li>
<li><b>Semester:</b> The 2 types of semester; either spring or autumn</li>
<li><b>Day:</b> The days</li>
<li><b>Activity:</b> The activites a time table uses, such as lecture and lab</li>
</ul>

<h2>Constraints</h2>
<ul>
<li><b>evaluationFormSummedEqualsOne:</b> This constraint is use as an EAnnotation in the CourseInstacne class and sums all the evaluation form weights to check if it equals one</li>
<li><b>workAndTimetableMatchingInHours:</b> This constraint is use as an EAnnotation in the CourseInstacne class and checks if the work description and the timetable are similar in number of lectures and labs. If a course does not have a timetable accessible, this constraint will return true. If number of lab hours in timetable extends the work, it will return true</li>
</ul>