/**
 */
package course;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Staff</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link course.Staff#getCoordinator <em>Coordinator</em>}</li>
 *   <li>{@link course.Staff#getLecturers <em>Lecturers</em>}</li>
 *   <li>{@link course.Staff#getOthers <em>Others</em>}</li>
 * </ul>
 *
 * @see course.CoursePackage#getStaff()
 * @model
 * @generated
 */
public interface Staff extends EObject {
	/**
	 * Returns the value of the '<em><b>Coordinator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coordinator</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coordinator</em>' reference.
	 * @see #setCoordinator(Person)
	 * @see course.CoursePackage#getStaff_Coordinator()
	 * @model
	 * @generated
	 */
	Person getCoordinator();

	/**
	 * Sets the value of the '{@link course.Staff#getCoordinator <em>Coordinator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Coordinator</em>' reference.
	 * @see #getCoordinator()
	 * @generated
	 */
	void setCoordinator(Person value);

	/**
	 * Returns the value of the '<em><b>Lecturers</b></em>' reference list.
	 * The list contents are of type {@link course.Person}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lecturers</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lecturers</em>' reference list.
	 * @see course.CoursePackage#getStaff_Lecturers()
	 * @model
	 * @generated
	 */
	EList<Person> getLecturers();

	/**
	 * Returns the value of the '<em><b>Others</b></em>' reference list.
	 * The list contents are of type {@link course.Person}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Others</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Others</em>' reference list.
	 * @see course.CoursePackage#getStaff_Others()
	 * @model
	 * @generated
	 */
	EList<Person> getOthers();

} // Staff
