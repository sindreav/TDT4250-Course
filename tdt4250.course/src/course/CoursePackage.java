/**
 */
package course;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see course.CourseFactory
 * @model kind="package"
 * @generated
 */
public interface CoursePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "course";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/course";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "course";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoursePackage eINSTANCE = course.impl.CoursePackageImpl.init();

	/**
	 * The meta object id for the '{@link course.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.impl.CourseImpl
	 * @see course.impl.CoursePackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 0;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CODE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDITS = 2;

	/**
	 * The feature id for the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__LEVEL = 3;

	/**
	 * The feature id for the '<em><b>Course Info</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__COURSE_INFO = 4;

	/**
	 * The feature id for the '<em><b>Department</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__DEPARTMENT = 5;

	/**
	 * The feature id for the '<em><b>Course Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__COURSE_INSTANCES = 6;

	/**
	 * The feature id for the '<em><b>Credit Reductions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDIT_REDUCTIONS = 7;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link course.impl.CourseInstanceImpl <em>Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.impl.CourseInstanceImpl
	 * @see course.impl.CoursePackageImpl#getCourseInstance()
	 * @generated
	 */
	int COURSE_INSTANCE = 1;

	/**
	 * The feature id for the '<em><b>Semester</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__SEMESTER = 0;

	/**
	 * The feature id for the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__YEAR = 1;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__LANGUAGE = 2;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__LOCATION = 3;

	/**
	 * The feature id for the '<em><b>Time Tables</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__TIME_TABLES = 4;

	/**
	 * The feature id for the '<em><b>Evaluation Forms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__EVALUATION_FORMS = 5;

	/**
	 * The feature id for the '<em><b>Course Work</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__COURSE_WORK = 6;

	/**
	 * The feature id for the '<em><b>Staff</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__STAFF = 7;

	/**
	 * The number of structural features of the '<em>Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link course.impl.CourseInfoImpl <em>Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.impl.CourseInfoImpl
	 * @see course.impl.CoursePackageImpl#getCourseInfo()
	 * @generated
	 */
	int COURSE_INFO = 2;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INFO__CONTENT = 0;

	/**
	 * The feature id for the '<em><b>Learning Outcome</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INFO__LEARNING_OUTCOME = 1;

	/**
	 * The feature id for the '<em><b>Learning Methods And Activities</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INFO__LEARNING_METHODS_AND_ACTIVITIES = 2;

	/**
	 * The feature id for the '<em><b>Compulsory Assignments</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INFO__COMPULSORY_ASSIGNMENTS = 3;

	/**
	 * The feature id for the '<em><b>Further On Evaluation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INFO__FURTHER_ON_EVALUATION = 4;

	/**
	 * The feature id for the '<em><b>Specific Conditions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INFO__SPECIFIC_CONDITIONS = 5;

	/**
	 * The feature id for the '<em><b>Recommended Previous Knowledge</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INFO__RECOMMENDED_PREVIOUS_KNOWLEDGE = 6;

	/**
	 * The feature id for the '<em><b>Course Materials</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INFO__COURSE_MATERIALS = 7;

	/**
	 * The number of structural features of the '<em>Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INFO_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link course.impl.TimetableImpl <em>Timetable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.impl.TimetableImpl
	 * @see course.impl.CoursePackageImpl#getTimetable()
	 * @generated
	 */
	int TIMETABLE = 6;

	/**
	 * The meta object id for the '{@link course.impl.CourseWorkImpl <em>Work</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.impl.CourseWorkImpl
	 * @see course.impl.CoursePackageImpl#getCourseWork()
	 * @generated
	 */
	int COURSE_WORK = 3;

	/**
	 * The feature id for the '<em><b>Lecture Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK__LECTURE_HOURS = 0;

	/**
	 * The feature id for the '<em><b>Lab Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK__LAB_HOURS = 1;

	/**
	 * The feature id for the '<em><b>Specialization Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK__SPECIALIZATION_HOURS = 2;

	/**
	 * The number of structural features of the '<em>Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link course.impl.CreditReductionImpl <em>Credit Reduction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.impl.CreditReductionImpl
	 * @see course.impl.CoursePackageImpl#getCreditReduction()
	 * @generated
	 */
	int CREDIT_REDUCTION = 4;

	/**
	 * The feature id for the '<em><b>Course Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION__COURSE_CODE = 0;

	/**
	 * The feature id for the '<em><b>Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION__REDUCTION = 1;

	/**
	 * The feature id for the '<em><b>From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION__FROM = 2;

	/**
	 * The feature id for the '<em><b>To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION__TO = 3;

	/**
	 * The number of structural features of the '<em>Credit Reduction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Credit Reduction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link course.impl.EvaluationFormImpl <em>Evaluation Form</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.impl.EvaluationFormImpl
	 * @see course.impl.CoursePackageImpl#getEvaluationForm()
	 * @generated
	 */
	int EVALUATION_FORM = 5;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FORM__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FORM__WEIGHT = 1;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FORM__DURATION = 2;

	/**
	 * The feature id for the '<em><b>Examination Aid</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FORM__EXAMINATION_AID = 3;

	/**
	 * The number of structural features of the '<em>Evaluation Form</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FORM_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Evaluation Form</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FORM_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Day</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__DAY = 0;

	/**
	 * The feature id for the '<em><b>Time From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__TIME_FROM = 1;

	/**
	 * The feature id for the '<em><b>Time To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__TIME_TO = 2;

	/**
	 * The feature id for the '<em><b>Week From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__WEEK_FROM = 3;

	/**
	 * The feature id for the '<em><b>Week To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__WEEK_TO = 4;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__ACTIVITY = 5;

	/**
	 * The feature id for the '<em><b>Planned For</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__PLANNED_FOR = 6;

	/**
	 * The feature id for the '<em><b>Room</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__ROOM = 7;

	/**
	 * The number of structural features of the '<em>Timetable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Timetable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link course.impl.TimeImpl <em>Time</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.impl.TimeImpl
	 * @see course.impl.CoursePackageImpl#getTime()
	 * @generated
	 */
	int TIME = 7;

	/**
	 * The feature id for the '<em><b>Hour</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME__HOUR = 0;

	/**
	 * The feature id for the '<em><b>Minute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME__MINUTE = 1;

	/**
	 * The number of structural features of the '<em>Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Get Difference</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME___GET_DIFFERENCE__TIME = 0;

	/**
	 * The number of operations of the '<em>Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link course.impl.PersonImpl <em>Person</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.impl.PersonImpl
	 * @see course.impl.CoursePackageImpl#getPerson()
	 * @generated
	 */
	int PERSON = 8;

	/**
	 * The feature id for the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__FIRST_NAME = 0;

	/**
	 * The feature id for the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__LAST_NAME = 1;

	/**
	 * The feature id for the '<em><b>Website</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__WEBSITE = 2;

	/**
	 * The number of structural features of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link course.impl.DepartmentImpl <em>Department</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.impl.DepartmentImpl
	 * @see course.impl.CoursePackageImpl#getDepartment()
	 * @generated
	 */
	int DEPARTMENT = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Website</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__WEBSITE = 1;

	/**
	 * The feature id for the '<em><b>Leader</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__LEADER = 2;

	/**
	 * The feature id for the '<em><b>Employees</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__EMPLOYEES = 3;

	/**
	 * The feature id for the '<em><b>Phone</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__PHONE = 4;

	/**
	 * The number of structural features of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link course.impl.StudyImpl <em>Study</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.impl.StudyImpl
	 * @see course.impl.CoursePackageImpl#getStudy()
	 * @generated
	 */
	int STUDY = 11;

	/**
	 * The meta object id for the '{@link course.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.impl.RoomImpl
	 * @see course.impl.CoursePackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 12;

	/**
	 * The meta object id for the '{@link course.impl.StaffImpl <em>Staff</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.impl.StaffImpl
	 * @see course.impl.CoursePackageImpl#getStaff()
	 * @generated
	 */
	int STAFF = 10;

	/**
	 * The feature id for the '<em><b>Coordinator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAFF__COORDINATOR = 0;

	/**
	 * The feature id for the '<em><b>Lecturers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAFF__LECTURERS = 1;

	/**
	 * The feature id for the '<em><b>Others</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAFF__OTHERS = 2;

	/**
	 * The number of structural features of the '<em>Staff</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAFF_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Staff</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAFF_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY__SHORT_NAME = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY__NAME = 1;

	/**
	 * The feature id for the '<em><b>Website</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY__WEBSITE = 2;

	/**
	 * The number of structural features of the '<em>Study</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Study</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__NAME = 0;

	/**
	 * The feature id for the '<em><b>Mazemap Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__MAZEMAP_URL = 1;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link course.impl.ExaminationAidImpl <em>Examination Aid</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.impl.ExaminationAidImpl
	 * @see course.impl.CoursePackageImpl#getExaminationAid()
	 * @generated
	 */
	int EXAMINATION_AID = 13;

	/**
	 * The feature id for the '<em><b>Char</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMINATION_AID__CHAR = 0;

	/**
	 * The feature id for the '<em><b>Info Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMINATION_AID__INFO_TEXT = 1;

	/**
	 * The feature id for the '<em><b>Website</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMINATION_AID__WEBSITE = 2;

	/**
	 * The number of structural features of the '<em>Examination Aid</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMINATION_AID_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Examination Aid</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMINATION_AID_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link course.EvaluationType <em>Evaluation Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.EvaluationType
	 * @see course.impl.CoursePackageImpl#getEvaluationType()
	 * @generated
	 */
	int EVALUATION_TYPE = 14;

	/**
	 * The meta object id for the '{@link course.Semester <em>Semester</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.Semester
	 * @see course.impl.CoursePackageImpl#getSemester()
	 * @generated
	 */
	int SEMESTER = 15;


	/**
	 * The meta object id for the '{@link course.Day <em>Day</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.Day
	 * @see course.impl.CoursePackageImpl#getDay()
	 * @generated
	 */
	int DAY = 16;

	/**
	 * The meta object id for the '{@link course.Activity <em>Activity</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see course.Activity
	 * @see course.impl.CoursePackageImpl#getActivity()
	 * @generated
	 */
	int ACTIVITY = 17;


	/**
	 * Returns the meta object for class '{@link course.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see course.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the attribute '{@link course.Course#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see course.Course#getCode()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Code();

	/**
	 * Returns the meta object for the attribute '{@link course.Course#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see course.Course#getName()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Name();

	/**
	 * Returns the meta object for the attribute '{@link course.Course#getCredits <em>Credits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits</em>'.
	 * @see course.Course#getCredits()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Credits();

	/**
	 * Returns the meta object for the attribute '{@link course.Course#getLevel <em>Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level</em>'.
	 * @see course.Course#getLevel()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Level();

	/**
	 * Returns the meta object for the reference '{@link course.Course#getCourseInfo <em>Course Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Course Info</em>'.
	 * @see course.Course#getCourseInfo()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_CourseInfo();

	/**
	 * Returns the meta object for the reference '{@link course.Course#getDepartment <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Department</em>'.
	 * @see course.Course#getDepartment()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Department();

	/**
	 * Returns the meta object for the reference list '{@link course.Course#getCourseInstances <em>Course Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Course Instances</em>'.
	 * @see course.Course#getCourseInstances()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_CourseInstances();

	/**
	 * Returns the meta object for the reference list '{@link course.Course#getCreditReductions <em>Credit Reductions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Credit Reductions</em>'.
	 * @see course.Course#getCreditReductions()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_CreditReductions();

	/**
	 * Returns the meta object for class '{@link course.CourseInstance <em>Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instance</em>'.
	 * @see course.CourseInstance
	 * @generated
	 */
	EClass getCourseInstance();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseInstance#getSemester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Semester</em>'.
	 * @see course.CourseInstance#getSemester()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Semester();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseInstance#getYear <em>Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Year</em>'.
	 * @see course.CourseInstance#getYear()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Year();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseInstance#getLanguage <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Language</em>'.
	 * @see course.CourseInstance#getLanguage()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Language();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseInstance#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see course.CourseInstance#getLocation()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Location();

	/**
	 * Returns the meta object for the reference list '{@link course.CourseInstance#getTimeTables <em>Time Tables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Time Tables</em>'.
	 * @see course.CourseInstance#getTimeTables()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_TimeTables();

	/**
	 * Returns the meta object for the reference list '{@link course.CourseInstance#getEvaluationForms <em>Evaluation Forms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Evaluation Forms</em>'.
	 * @see course.CourseInstance#getEvaluationForms()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_EvaluationForms();

	/**
	 * Returns the meta object for the reference '{@link course.CourseInstance#getCourseWork <em>Course Work</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Course Work</em>'.
	 * @see course.CourseInstance#getCourseWork()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_CourseWork();

	/**
	 * Returns the meta object for the reference '{@link course.CourseInstance#getStaff <em>Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Staff</em>'.
	 * @see course.CourseInstance#getStaff()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Staff();

	/**
	 * Returns the meta object for class '{@link course.CourseInfo <em>Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Info</em>'.
	 * @see course.CourseInfo
	 * @generated
	 */
	EClass getCourseInfo();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseInfo#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see course.CourseInfo#getContent()
	 * @see #getCourseInfo()
	 * @generated
	 */
	EAttribute getCourseInfo_Content();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseInfo#getLearningOutcome <em>Learning Outcome</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Learning Outcome</em>'.
	 * @see course.CourseInfo#getLearningOutcome()
	 * @see #getCourseInfo()
	 * @generated
	 */
	EAttribute getCourseInfo_LearningOutcome();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseInfo#getLearningMethodsAndActivities <em>Learning Methods And Activities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Learning Methods And Activities</em>'.
	 * @see course.CourseInfo#getLearningMethodsAndActivities()
	 * @see #getCourseInfo()
	 * @generated
	 */
	EAttribute getCourseInfo_LearningMethodsAndActivities();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseInfo#getCompulsoryAssignments <em>Compulsory Assignments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Compulsory Assignments</em>'.
	 * @see course.CourseInfo#getCompulsoryAssignments()
	 * @see #getCourseInfo()
	 * @generated
	 */
	EAttribute getCourseInfo_CompulsoryAssignments();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseInfo#getFurtherOnEvaluation <em>Further On Evaluation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Further On Evaluation</em>'.
	 * @see course.CourseInfo#getFurtherOnEvaluation()
	 * @see #getCourseInfo()
	 * @generated
	 */
	EAttribute getCourseInfo_FurtherOnEvaluation();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseInfo#getSpecificConditions <em>Specific Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Specific Conditions</em>'.
	 * @see course.CourseInfo#getSpecificConditions()
	 * @see #getCourseInfo()
	 * @generated
	 */
	EAttribute getCourseInfo_SpecificConditions();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseInfo#getRecommendedPreviousKnowledge <em>Recommended Previous Knowledge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Recommended Previous Knowledge</em>'.
	 * @see course.CourseInfo#getRecommendedPreviousKnowledge()
	 * @see #getCourseInfo()
	 * @generated
	 */
	EAttribute getCourseInfo_RecommendedPreviousKnowledge();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseInfo#getCourseMaterials <em>Course Materials</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Course Materials</em>'.
	 * @see course.CourseInfo#getCourseMaterials()
	 * @see #getCourseInfo()
	 * @generated
	 */
	EAttribute getCourseInfo_CourseMaterials();

	/**
	 * Returns the meta object for class '{@link course.Timetable <em>Timetable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timetable</em>'.
	 * @see course.Timetable
	 * @generated
	 */
	EClass getTimetable();

	/**
	 * Returns the meta object for the attribute '{@link course.Timetable#getDay <em>Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Day</em>'.
	 * @see course.Timetable#getDay()
	 * @see #getTimetable()
	 * @generated
	 */
	EAttribute getTimetable_Day();

	/**
	 * Returns the meta object for the reference '{@link course.Timetable#getTimeFrom <em>Time From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Time From</em>'.
	 * @see course.Timetable#getTimeFrom()
	 * @see #getTimetable()
	 * @generated
	 */
	EReference getTimetable_TimeFrom();

	/**
	 * Returns the meta object for the reference '{@link course.Timetable#getTimeTo <em>Time To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Time To</em>'.
	 * @see course.Timetable#getTimeTo()
	 * @see #getTimetable()
	 * @generated
	 */
	EReference getTimetable_TimeTo();

	/**
	 * Returns the meta object for the attribute '{@link course.Timetable#getWeekFrom <em>Week From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Week From</em>'.
	 * @see course.Timetable#getWeekFrom()
	 * @see #getTimetable()
	 * @generated
	 */
	EAttribute getTimetable_WeekFrom();

	/**
	 * Returns the meta object for the attribute '{@link course.Timetable#getWeekTo <em>Week To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Week To</em>'.
	 * @see course.Timetable#getWeekTo()
	 * @see #getTimetable()
	 * @generated
	 */
	EAttribute getTimetable_WeekTo();

	/**
	 * Returns the meta object for the attribute '{@link course.Timetable#getActivity <em>Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Activity</em>'.
	 * @see course.Timetable#getActivity()
	 * @see #getTimetable()
	 * @generated
	 */
	EAttribute getTimetable_Activity();

	/**
	 * Returns the meta object for the reference list '{@link course.Timetable#getPlannedFor <em>Planned For</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Planned For</em>'.
	 * @see course.Timetable#getPlannedFor()
	 * @see #getTimetable()
	 * @generated
	 */
	EReference getTimetable_PlannedFor();

	/**
	 * Returns the meta object for the reference '{@link course.Timetable#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room</em>'.
	 * @see course.Timetable#getRoom()
	 * @see #getTimetable()
	 * @generated
	 */
	EReference getTimetable_Room();

	/**
	 * Returns the meta object for class '{@link course.CourseWork <em>Work</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Work</em>'.
	 * @see course.CourseWork
	 * @generated
	 */
	EClass getCourseWork();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseWork#getLectureHours <em>Lecture Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lecture Hours</em>'.
	 * @see course.CourseWork#getLectureHours()
	 * @see #getCourseWork()
	 * @generated
	 */
	EAttribute getCourseWork_LectureHours();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseWork#getLabHours <em>Lab Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lab Hours</em>'.
	 * @see course.CourseWork#getLabHours()
	 * @see #getCourseWork()
	 * @generated
	 */
	EAttribute getCourseWork_LabHours();

	/**
	 * Returns the meta object for the attribute '{@link course.CourseWork#getSpecializationHours <em>Specialization Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Specialization Hours</em>'.
	 * @see course.CourseWork#getSpecializationHours()
	 * @see #getCourseWork()
	 * @generated
	 */
	EAttribute getCourseWork_SpecializationHours();

	/**
	 * Returns the meta object for class '{@link course.CreditReduction <em>Credit Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Credit Reduction</em>'.
	 * @see course.CreditReduction
	 * @generated
	 */
	EClass getCreditReduction();

	/**
	 * Returns the meta object for the attribute '{@link course.CreditReduction#getCourseCode <em>Course Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Course Code</em>'.
	 * @see course.CreditReduction#getCourseCode()
	 * @see #getCreditReduction()
	 * @generated
	 */
	EAttribute getCreditReduction_CourseCode();

	/**
	 * Returns the meta object for the attribute '{@link course.CreditReduction#getReduction <em>Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reduction</em>'.
	 * @see course.CreditReduction#getReduction()
	 * @see #getCreditReduction()
	 * @generated
	 */
	EAttribute getCreditReduction_Reduction();

	/**
	 * Returns the meta object for the attribute '{@link course.CreditReduction#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>From</em>'.
	 * @see course.CreditReduction#getFrom()
	 * @see #getCreditReduction()
	 * @generated
	 */
	EAttribute getCreditReduction_From();

	/**
	 * Returns the meta object for the attribute '{@link course.CreditReduction#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To</em>'.
	 * @see course.CreditReduction#getTo()
	 * @see #getCreditReduction()
	 * @generated
	 */
	EAttribute getCreditReduction_To();

	/**
	 * Returns the meta object for class '{@link course.EvaluationForm <em>Evaluation Form</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evaluation Form</em>'.
	 * @see course.EvaluationForm
	 * @generated
	 */
	EClass getEvaluationForm();

	/**
	 * Returns the meta object for the attribute '{@link course.EvaluationForm#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see course.EvaluationForm#getType()
	 * @see #getEvaluationForm()
	 * @generated
	 */
	EAttribute getEvaluationForm_Type();

	/**
	 * Returns the meta object for the attribute '{@link course.EvaluationForm#getWeight <em>Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Weight</em>'.
	 * @see course.EvaluationForm#getWeight()
	 * @see #getEvaluationForm()
	 * @generated
	 */
	EAttribute getEvaluationForm_Weight();

	/**
	 * Returns the meta object for the attribute '{@link course.EvaluationForm#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see course.EvaluationForm#getDuration()
	 * @see #getEvaluationForm()
	 * @generated
	 */
	EAttribute getEvaluationForm_Duration();

	/**
	 * Returns the meta object for the reference '{@link course.EvaluationForm#getExaminationAid <em>Examination Aid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Examination Aid</em>'.
	 * @see course.EvaluationForm#getExaminationAid()
	 * @see #getEvaluationForm()
	 * @generated
	 */
	EReference getEvaluationForm_ExaminationAid();

	/**
	 * Returns the meta object for class '{@link course.Time <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time</em>'.
	 * @see course.Time
	 * @generated
	 */
	EClass getTime();

	/**
	 * Returns the meta object for the attribute '{@link course.Time#getHour <em>Hour</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hour</em>'.
	 * @see course.Time#getHour()
	 * @see #getTime()
	 * @generated
	 */
	EAttribute getTime_Hour();

	/**
	 * Returns the meta object for the attribute '{@link course.Time#getMinute <em>Minute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minute</em>'.
	 * @see course.Time#getMinute()
	 * @see #getTime()
	 * @generated
	 */
	EAttribute getTime_Minute();

	/**
	 * Returns the meta object for the '{@link course.Time#getDifference(course.Time) <em>Get Difference</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Difference</em>' operation.
	 * @see course.Time#getDifference(course.Time)
	 * @generated
	 */
	EOperation getTime__GetDifference__Time();

	/**
	 * Returns the meta object for class '{@link course.Person <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person</em>'.
	 * @see course.Person
	 * @generated
	 */
	EClass getPerson();

	/**
	 * Returns the meta object for the attribute '{@link course.Person#getFirstName <em>First Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Name</em>'.
	 * @see course.Person#getFirstName()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_FirstName();

	/**
	 * Returns the meta object for the attribute '{@link course.Person#getLastName <em>Last Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Name</em>'.
	 * @see course.Person#getLastName()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_LastName();

	/**
	 * Returns the meta object for the attribute '{@link course.Person#getWebsite <em>Website</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Website</em>'.
	 * @see course.Person#getWebsite()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_Website();

	/**
	 * Returns the meta object for class '{@link course.Department <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Department</em>'.
	 * @see course.Department
	 * @generated
	 */
	EClass getDepartment();

	/**
	 * Returns the meta object for the attribute '{@link course.Department#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see course.Department#getName()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_Name();

	/**
	 * Returns the meta object for the attribute '{@link course.Department#getWebsite <em>Website</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Website</em>'.
	 * @see course.Department#getWebsite()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_Website();

	/**
	 * Returns the meta object for the reference '{@link course.Department#getLeader <em>Leader</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Leader</em>'.
	 * @see course.Department#getLeader()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Leader();

	/**
	 * Returns the meta object for the reference list '{@link course.Department#getEmployees <em>Employees</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Employees</em>'.
	 * @see course.Department#getEmployees()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Employees();

	/**
	 * Returns the meta object for the attribute '{@link course.Department#getPhone <em>Phone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Phone</em>'.
	 * @see course.Department#getPhone()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_Phone();

	/**
	 * Returns the meta object for class '{@link course.Study <em>Study</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Study</em>'.
	 * @see course.Study
	 * @generated
	 */
	EClass getStudy();

	/**
	 * Returns the meta object for the attribute '{@link course.Study#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see course.Study#getName()
	 * @see #getStudy()
	 * @generated
	 */
	EAttribute getStudy_Name();

	/**
	 * Returns the meta object for the attribute '{@link course.Study#getShortName <em>Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Short Name</em>'.
	 * @see course.Study#getShortName()
	 * @see #getStudy()
	 * @generated
	 */
	EAttribute getStudy_ShortName();

	/**
	 * Returns the meta object for the attribute '{@link course.Study#getWebsite <em>Website</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Website</em>'.
	 * @see course.Study#getWebsite()
	 * @see #getStudy()
	 * @generated
	 */
	EAttribute getStudy_Website();

	/**
	 * Returns the meta object for class '{@link course.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see course.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the attribute '{@link course.Room#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see course.Room#getName()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_Name();

	/**
	 * Returns the meta object for the attribute '{@link course.Room#getMazemapUrl <em>Mazemap Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mazemap Url</em>'.
	 * @see course.Room#getMazemapUrl()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_MazemapUrl();

	/**
	 * Returns the meta object for class '{@link course.ExaminationAid <em>Examination Aid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Examination Aid</em>'.
	 * @see course.ExaminationAid
	 * @generated
	 */
	EClass getExaminationAid();

	/**
	 * Returns the meta object for the attribute '{@link course.ExaminationAid#getChar <em>Char</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Char</em>'.
	 * @see course.ExaminationAid#getChar()
	 * @see #getExaminationAid()
	 * @generated
	 */
	EAttribute getExaminationAid_Char();

	/**
	 * Returns the meta object for the attribute '{@link course.ExaminationAid#getInfoText <em>Info Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Info Text</em>'.
	 * @see course.ExaminationAid#getInfoText()
	 * @see #getExaminationAid()
	 * @generated
	 */
	EAttribute getExaminationAid_InfoText();

	/**
	 * Returns the meta object for the attribute '{@link course.ExaminationAid#getWebsite <em>Website</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Website</em>'.
	 * @see course.ExaminationAid#getWebsite()
	 * @see #getExaminationAid()
	 * @generated
	 */
	EAttribute getExaminationAid_Website();

	/**
	 * Returns the meta object for class '{@link course.Staff <em>Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Staff</em>'.
	 * @see course.Staff
	 * @generated
	 */
	EClass getStaff();

	/**
	 * Returns the meta object for the reference '{@link course.Staff#getCoordinator <em>Coordinator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Coordinator</em>'.
	 * @see course.Staff#getCoordinator()
	 * @see #getStaff()
	 * @generated
	 */
	EReference getStaff_Coordinator();

	/**
	 * Returns the meta object for the reference list '{@link course.Staff#getLecturers <em>Lecturers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Lecturers</em>'.
	 * @see course.Staff#getLecturers()
	 * @see #getStaff()
	 * @generated
	 */
	EReference getStaff_Lecturers();

	/**
	 * Returns the meta object for the reference list '{@link course.Staff#getOthers <em>Others</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Others</em>'.
	 * @see course.Staff#getOthers()
	 * @see #getStaff()
	 * @generated
	 */
	EReference getStaff_Others();

	/**
	 * Returns the meta object for enum '{@link course.EvaluationType <em>Evaluation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Evaluation Type</em>'.
	 * @see course.EvaluationType
	 * @generated
	 */
	EEnum getEvaluationType();

	/**
	 * Returns the meta object for enum '{@link course.Semester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Semester</em>'.
	 * @see course.Semester
	 * @generated
	 */
	EEnum getSemester();

	/**
	 * Returns the meta object for enum '{@link course.Day <em>Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Day</em>'.
	 * @see course.Day
	 * @generated
	 */
	EEnum getDay();

	/**
	 * Returns the meta object for enum '{@link course.Activity <em>Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Activity</em>'.
	 * @see course.Activity
	 * @generated
	 */
	EEnum getActivity();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CourseFactory getCourseFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link course.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.impl.CourseImpl
		 * @see course.impl.CoursePackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CODE = eINSTANCE.getCourse_Code();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__NAME = eINSTANCE.getCourse_Name();

		/**
		 * The meta object literal for the '<em><b>Credits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CREDITS = eINSTANCE.getCourse_Credits();

		/**
		 * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__LEVEL = eINSTANCE.getCourse_Level();

		/**
		 * The meta object literal for the '<em><b>Course Info</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__COURSE_INFO = eINSTANCE.getCourse_CourseInfo();

		/**
		 * The meta object literal for the '<em><b>Department</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__DEPARTMENT = eINSTANCE.getCourse_Department();

		/**
		 * The meta object literal for the '<em><b>Course Instances</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__COURSE_INSTANCES = eINSTANCE.getCourse_CourseInstances();

		/**
		 * The meta object literal for the '<em><b>Credit Reductions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__CREDIT_REDUCTIONS = eINSTANCE.getCourse_CreditReductions();

		/**
		 * The meta object literal for the '{@link course.impl.CourseInstanceImpl <em>Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.impl.CourseInstanceImpl
		 * @see course.impl.CoursePackageImpl#getCourseInstance()
		 * @generated
		 */
		EClass COURSE_INSTANCE = eINSTANCE.getCourseInstance();

		/**
		 * The meta object literal for the '<em><b>Semester</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__SEMESTER = eINSTANCE.getCourseInstance_Semester();

		/**
		 * The meta object literal for the '<em><b>Year</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__YEAR = eINSTANCE.getCourseInstance_Year();

		/**
		 * The meta object literal for the '<em><b>Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__LANGUAGE = eINSTANCE.getCourseInstance_Language();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__LOCATION = eINSTANCE.getCourseInstance_Location();

		/**
		 * The meta object literal for the '<em><b>Time Tables</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__TIME_TABLES = eINSTANCE.getCourseInstance_TimeTables();

		/**
		 * The meta object literal for the '<em><b>Evaluation Forms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__EVALUATION_FORMS = eINSTANCE.getCourseInstance_EvaluationForms();

		/**
		 * The meta object literal for the '<em><b>Course Work</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__COURSE_WORK = eINSTANCE.getCourseInstance_CourseWork();

		/**
		 * The meta object literal for the '<em><b>Staff</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__STAFF = eINSTANCE.getCourseInstance_Staff();

		/**
		 * The meta object literal for the '{@link course.impl.CourseInfoImpl <em>Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.impl.CourseInfoImpl
		 * @see course.impl.CoursePackageImpl#getCourseInfo()
		 * @generated
		 */
		EClass COURSE_INFO = eINSTANCE.getCourseInfo();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INFO__CONTENT = eINSTANCE.getCourseInfo_Content();

		/**
		 * The meta object literal for the '<em><b>Learning Outcome</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INFO__LEARNING_OUTCOME = eINSTANCE.getCourseInfo_LearningOutcome();

		/**
		 * The meta object literal for the '<em><b>Learning Methods And Activities</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INFO__LEARNING_METHODS_AND_ACTIVITIES = eINSTANCE.getCourseInfo_LearningMethodsAndActivities();

		/**
		 * The meta object literal for the '<em><b>Compulsory Assignments</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INFO__COMPULSORY_ASSIGNMENTS = eINSTANCE.getCourseInfo_CompulsoryAssignments();

		/**
		 * The meta object literal for the '<em><b>Further On Evaluation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INFO__FURTHER_ON_EVALUATION = eINSTANCE.getCourseInfo_FurtherOnEvaluation();

		/**
		 * The meta object literal for the '<em><b>Specific Conditions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INFO__SPECIFIC_CONDITIONS = eINSTANCE.getCourseInfo_SpecificConditions();

		/**
		 * The meta object literal for the '<em><b>Recommended Previous Knowledge</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INFO__RECOMMENDED_PREVIOUS_KNOWLEDGE = eINSTANCE.getCourseInfo_RecommendedPreviousKnowledge();

		/**
		 * The meta object literal for the '<em><b>Course Materials</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INFO__COURSE_MATERIALS = eINSTANCE.getCourseInfo_CourseMaterials();

		/**
		 * The meta object literal for the '{@link course.impl.TimetableImpl <em>Timetable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.impl.TimetableImpl
		 * @see course.impl.CoursePackageImpl#getTimetable()
		 * @generated
		 */
		EClass TIMETABLE = eINSTANCE.getTimetable();

		/**
		 * The meta object literal for the '<em><b>Day</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMETABLE__DAY = eINSTANCE.getTimetable_Day();

		/**
		 * The meta object literal for the '<em><b>Time From</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMETABLE__TIME_FROM = eINSTANCE.getTimetable_TimeFrom();

		/**
		 * The meta object literal for the '<em><b>Time To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMETABLE__TIME_TO = eINSTANCE.getTimetable_TimeTo();

		/**
		 * The meta object literal for the '<em><b>Week From</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMETABLE__WEEK_FROM = eINSTANCE.getTimetable_WeekFrom();

		/**
		 * The meta object literal for the '<em><b>Week To</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMETABLE__WEEK_TO = eINSTANCE.getTimetable_WeekTo();

		/**
		 * The meta object literal for the '<em><b>Activity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMETABLE__ACTIVITY = eINSTANCE.getTimetable_Activity();

		/**
		 * The meta object literal for the '<em><b>Planned For</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMETABLE__PLANNED_FOR = eINSTANCE.getTimetable_PlannedFor();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMETABLE__ROOM = eINSTANCE.getTimetable_Room();

		/**
		 * The meta object literal for the '{@link course.impl.CourseWorkImpl <em>Work</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.impl.CourseWorkImpl
		 * @see course.impl.CoursePackageImpl#getCourseWork()
		 * @generated
		 */
		EClass COURSE_WORK = eINSTANCE.getCourseWork();

		/**
		 * The meta object literal for the '<em><b>Lecture Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_WORK__LECTURE_HOURS = eINSTANCE.getCourseWork_LectureHours();

		/**
		 * The meta object literal for the '<em><b>Lab Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_WORK__LAB_HOURS = eINSTANCE.getCourseWork_LabHours();

		/**
		 * The meta object literal for the '<em><b>Specialization Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_WORK__SPECIALIZATION_HOURS = eINSTANCE.getCourseWork_SpecializationHours();

		/**
		 * The meta object literal for the '{@link course.impl.CreditReductionImpl <em>Credit Reduction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.impl.CreditReductionImpl
		 * @see course.impl.CoursePackageImpl#getCreditReduction()
		 * @generated
		 */
		EClass CREDIT_REDUCTION = eINSTANCE.getCreditReduction();

		/**
		 * The meta object literal for the '<em><b>Course Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREDIT_REDUCTION__COURSE_CODE = eINSTANCE.getCreditReduction_CourseCode();

		/**
		 * The meta object literal for the '<em><b>Reduction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREDIT_REDUCTION__REDUCTION = eINSTANCE.getCreditReduction_Reduction();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREDIT_REDUCTION__FROM = eINSTANCE.getCreditReduction_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREDIT_REDUCTION__TO = eINSTANCE.getCreditReduction_To();

		/**
		 * The meta object literal for the '{@link course.impl.EvaluationFormImpl <em>Evaluation Form</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.impl.EvaluationFormImpl
		 * @see course.impl.CoursePackageImpl#getEvaluationForm()
		 * @generated
		 */
		EClass EVALUATION_FORM = eINSTANCE.getEvaluationForm();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_FORM__TYPE = eINSTANCE.getEvaluationForm_Type();

		/**
		 * The meta object literal for the '<em><b>Weight</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_FORM__WEIGHT = eINSTANCE.getEvaluationForm_Weight();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_FORM__DURATION = eINSTANCE.getEvaluationForm_Duration();

		/**
		 * The meta object literal for the '<em><b>Examination Aid</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVALUATION_FORM__EXAMINATION_AID = eINSTANCE.getEvaluationForm_ExaminationAid();

		/**
		 * The meta object literal for the '{@link course.impl.TimeImpl <em>Time</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.impl.TimeImpl
		 * @see course.impl.CoursePackageImpl#getTime()
		 * @generated
		 */
		EClass TIME = eINSTANCE.getTime();

		/**
		 * The meta object literal for the '<em><b>Hour</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME__HOUR = eINSTANCE.getTime_Hour();

		/**
		 * The meta object literal for the '<em><b>Minute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME__MINUTE = eINSTANCE.getTime_Minute();

		/**
		 * The meta object literal for the '<em><b>Get Difference</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TIME___GET_DIFFERENCE__TIME = eINSTANCE.getTime__GetDifference__Time();

		/**
		 * The meta object literal for the '{@link course.impl.PersonImpl <em>Person</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.impl.PersonImpl
		 * @see course.impl.CoursePackageImpl#getPerson()
		 * @generated
		 */
		EClass PERSON = eINSTANCE.getPerson();

		/**
		 * The meta object literal for the '<em><b>First Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__FIRST_NAME = eINSTANCE.getPerson_FirstName();

		/**
		 * The meta object literal for the '<em><b>Last Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__LAST_NAME = eINSTANCE.getPerson_LastName();

		/**
		 * The meta object literal for the '<em><b>Website</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__WEBSITE = eINSTANCE.getPerson_Website();

		/**
		 * The meta object literal for the '{@link course.impl.DepartmentImpl <em>Department</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.impl.DepartmentImpl
		 * @see course.impl.CoursePackageImpl#getDepartment()
		 * @generated
		 */
		EClass DEPARTMENT = eINSTANCE.getDepartment();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__NAME = eINSTANCE.getDepartment_Name();

		/**
		 * The meta object literal for the '<em><b>Website</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__WEBSITE = eINSTANCE.getDepartment_Website();

		/**
		 * The meta object literal for the '<em><b>Leader</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__LEADER = eINSTANCE.getDepartment_Leader();

		/**
		 * The meta object literal for the '<em><b>Employees</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__EMPLOYEES = eINSTANCE.getDepartment_Employees();

		/**
		 * The meta object literal for the '<em><b>Phone</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__PHONE = eINSTANCE.getDepartment_Phone();

		/**
		 * The meta object literal for the '{@link course.impl.StudyImpl <em>Study</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.impl.StudyImpl
		 * @see course.impl.CoursePackageImpl#getStudy()
		 * @generated
		 */
		EClass STUDY = eINSTANCE.getStudy();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY__NAME = eINSTANCE.getStudy_Name();

		/**
		 * The meta object literal for the '<em><b>Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY__SHORT_NAME = eINSTANCE.getStudy_ShortName();

		/**
		 * The meta object literal for the '<em><b>Website</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY__WEBSITE = eINSTANCE.getStudy_Website();

		/**
		 * The meta object literal for the '{@link course.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.impl.RoomImpl
		 * @see course.impl.CoursePackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__NAME = eINSTANCE.getRoom_Name();

		/**
		 * The meta object literal for the '<em><b>Mazemap Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__MAZEMAP_URL = eINSTANCE.getRoom_MazemapUrl();

		/**
		 * The meta object literal for the '{@link course.impl.ExaminationAidImpl <em>Examination Aid</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.impl.ExaminationAidImpl
		 * @see course.impl.CoursePackageImpl#getExaminationAid()
		 * @generated
		 */
		EClass EXAMINATION_AID = eINSTANCE.getExaminationAid();

		/**
		 * The meta object literal for the '<em><b>Char</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXAMINATION_AID__CHAR = eINSTANCE.getExaminationAid_Char();

		/**
		 * The meta object literal for the '<em><b>Info Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXAMINATION_AID__INFO_TEXT = eINSTANCE.getExaminationAid_InfoText();

		/**
		 * The meta object literal for the '<em><b>Website</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXAMINATION_AID__WEBSITE = eINSTANCE.getExaminationAid_Website();

		/**
		 * The meta object literal for the '{@link course.impl.StaffImpl <em>Staff</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.impl.StaffImpl
		 * @see course.impl.CoursePackageImpl#getStaff()
		 * @generated
		 */
		EClass STAFF = eINSTANCE.getStaff();

		/**
		 * The meta object literal for the '<em><b>Coordinator</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STAFF__COORDINATOR = eINSTANCE.getStaff_Coordinator();

		/**
		 * The meta object literal for the '<em><b>Lecturers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STAFF__LECTURERS = eINSTANCE.getStaff_Lecturers();

		/**
		 * The meta object literal for the '<em><b>Others</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STAFF__OTHERS = eINSTANCE.getStaff_Others();

		/**
		 * The meta object literal for the '{@link course.EvaluationType <em>Evaluation Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.EvaluationType
		 * @see course.impl.CoursePackageImpl#getEvaluationType()
		 * @generated
		 */
		EEnum EVALUATION_TYPE = eINSTANCE.getEvaluationType();

		/**
		 * The meta object literal for the '{@link course.Semester <em>Semester</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.Semester
		 * @see course.impl.CoursePackageImpl#getSemester()
		 * @generated
		 */
		EEnum SEMESTER = eINSTANCE.getSemester();

		/**
		 * The meta object literal for the '{@link course.Day <em>Day</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.Day
		 * @see course.impl.CoursePackageImpl#getDay()
		 * @generated
		 */
		EEnum DAY = eINSTANCE.getDay();

		/**
		 * The meta object literal for the '{@link course.Activity <em>Activity</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see course.Activity
		 * @see course.impl.CoursePackageImpl#getActivity()
		 * @generated
		 */
		EEnum ACTIVITY = eINSTANCE.getActivity();

	}

} //CoursePackage
