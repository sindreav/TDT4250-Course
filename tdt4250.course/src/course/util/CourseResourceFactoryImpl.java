package course.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;

public class CourseResourceFactoryImpl extends ResourceFactoryImpl{

	public CourseResourceFactoryImpl() {
		super();
	}
	
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Resource createResource(URI uri) {
		Resource result = new CourseResourceImpl(uri);
		return result;
	}

	
}
