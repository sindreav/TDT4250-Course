/**
 */
package course.util;

import course.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see course.CoursePackage
 * @generated
 */
public class CourseValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final CourseValidator INSTANCE = new CourseValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "course";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return CoursePackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case CoursePackage.COURSE:
				return validateCourse((Course)value, diagnostics, context);
			case CoursePackage.COURSE_INSTANCE:
				return validateCourseInstance((CourseInstance)value, diagnostics, context);
			case CoursePackage.COURSE_INFO:
				return validateCourseInfo((CourseInfo)value, diagnostics, context);
			case CoursePackage.COURSE_WORK:
				return validateCourseWork((CourseWork)value, diagnostics, context);
			case CoursePackage.CREDIT_REDUCTION:
				return validateCreditReduction((CreditReduction)value, diagnostics, context);
			case CoursePackage.EVALUATION_FORM:
				return validateEvaluationForm((EvaluationForm)value, diagnostics, context);
			case CoursePackage.TIMETABLE:
				return validateTimetable((Timetable)value, diagnostics, context);
			case CoursePackage.TIME:
				return validateTime((Time)value, diagnostics, context);
			case CoursePackage.PERSON:
				return validatePerson((Person)value, diagnostics, context);
			case CoursePackage.DEPARTMENT:
				return validateDepartment((Department)value, diagnostics, context);
			case CoursePackage.STAFF:
				return validateStaff((Staff)value, diagnostics, context);
			case CoursePackage.STUDY:
				return validateStudy((Study)value, diagnostics, context);
			case CoursePackage.ROOM:
				return validateRoom((Room)value, diagnostics, context);
			case CoursePackage.EXAMINATION_AID:
				return validateExaminationAid((ExaminationAid)value, diagnostics, context);
			case CoursePackage.EVALUATION_TYPE:
				return validateEvaluationType((EvaluationType)value, diagnostics, context);
			case CoursePackage.SEMESTER:
				return validateSemester((Semester)value, diagnostics, context);
			case CoursePackage.DAY:
				return validateDay((Day)value, diagnostics, context);
			case CoursePackage.ACTIVITY:
				return validateActivity((Activity)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(course, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInstance(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(courseInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourseInstance_evaluationFormSummedEqualsOne(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourseInstance_workAndTimetableMatchingInHours(courseInstance, diagnostics, context);
		return result;
	}

	/**
	 * Validates the evaluationFormSummedEqualsOne constraint of '<em>Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateCourseInstance_evaluationFormSummedEqualsOne(CourseInstance courseInstance,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		double totalWeight = 0;
		for (int i = 0; i < courseInstance.getEvaluationForms().size(); i++) {
			totalWeight += courseInstance.getEvaluationForms().get(i).getWeight();
		}

		if (totalWeight != 1) {
			if (diagnostics != null) {
				diagnostics.add(createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0,
						"_UI_GenericConstraint_diagnostic",
						new Object[] { "evaluationFormSummedEqualsOne", getObjectLabel(courseInstance, context) },
						new Object[] { courseInstance }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the workAndTimetableMatchingInHours constraint of '<em>Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateCourseInstance_workAndTimetableMatchingInHours(CourseInstance courseInstance,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!(courseInstance.getCourseWork() != null && courseInstance.getTimeTables() != null))
			return true;
		int workLectures = courseInstance.getCourseWork().getLectureHours();
		int workLabs = courseInstance.getCourseWork().getLabHours();
		List<Timetable> list = courseInstance.getTimeTables();
		Map<Integer, Integer> lectureMap = new HashMap<Integer, Integer>();
		Map<Integer, Integer> labMap = new HashMap<Integer, Integer>();

		Boolean lecturesCorrect = true;
		Boolean labsCorrect = true;

		for (int i = 0; i < list.size(); i++) {
			Timetable t = list.get(i);
			for (int j = t.getWeekFrom(); j <= t.getWeekTo(); j++) {
				int totalTime = t.getTimeFrom().getDifference(t.getTimeTo());
				int numPauses = (int) (Math.floor(totalTime/45)-1);
				int time = totalTime - (15*numPauses);
				if (t.getActivity().getLiteral().equals("Forelesning")) {
					if (lectureMap.get(j) == null) {
						lectureMap.put(j, time);
					} else {
						lectureMap.replace(j, lectureMap.get(j) + time);
					}
				} else if (t.getActivity().getLiteral().equals("�ving")) {
					if (labMap.get(j) == null) {
						labMap.put(j, time);
					} else {
						labMap.replace(j, labMap.get(j) + time);
					}
				}
			}

		}
		
		for (int key : lectureMap.keySet()) {
			if (lectureMap.get(key) != (workLectures * 45)) {
				lecturesCorrect = false;
				break;
			}
		}

		for (int key : labMap.keySet()) {
			if (labMap.get(key) < (workLabs * 45)) {
				labsCorrect = false;
				break;
			}
		}

		if (!lecturesCorrect || !labsCorrect) {
			if (diagnostics != null) {
				diagnostics.add(createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0,
						"_UI_GenericConstraint_diagnostic",
						new Object[] { "workAndTimetableMatchingInHours", getObjectLabel(courseInstance, context) },
						new Object[] { courseInstance }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInfo(CourseInfo courseInfo, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(courseInfo, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTimetable(Timetable timetable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(timetable, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseWork(CourseWork courseWork, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(courseWork, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCreditReduction(CreditReduction creditReduction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(creditReduction, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEvaluationForm(EvaluationForm evaluationForm, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(evaluationForm, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTime(Time time, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(time, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePerson(Person person, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(person, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDepartment(Department department, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(department, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStudy(Study study, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(study, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRoom(Room room, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(room, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExaminationAid(ExaminationAid examinationAid, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(examinationAid, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStaff(Staff staff, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(staff, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEvaluationType(EvaluationType evaluationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSemester(Semester semester, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDay(Day day, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateActivity(Activity activity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //CourseValidator
