/**
 */
package course;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timetable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link course.Timetable#getDay <em>Day</em>}</li>
 *   <li>{@link course.Timetable#getTimeFrom <em>Time From</em>}</li>
 *   <li>{@link course.Timetable#getTimeTo <em>Time To</em>}</li>
 *   <li>{@link course.Timetable#getWeekFrom <em>Week From</em>}</li>
 *   <li>{@link course.Timetable#getWeekTo <em>Week To</em>}</li>
 *   <li>{@link course.Timetable#getActivity <em>Activity</em>}</li>
 *   <li>{@link course.Timetable#getPlannedFor <em>Planned For</em>}</li>
 *   <li>{@link course.Timetable#getRoom <em>Room</em>}</li>
 * </ul>
 *
 * @see course.CoursePackage#getTimetable()
 * @model
 * @generated
 */
public interface Timetable extends EObject {
	/**
	 * Returns the value of the '<em><b>Day</b></em>' attribute.
	 * The literals are from the enumeration {@link course.Day}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Day</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Day</em>' attribute.
	 * @see course.Day
	 * @see #setDay(Day)
	 * @see course.CoursePackage#getTimetable_Day()
	 * @model
	 * @generated
	 */
	Day getDay();

	/**
	 * Sets the value of the '{@link course.Timetable#getDay <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Day</em>' attribute.
	 * @see course.Day
	 * @see #getDay()
	 * @generated
	 */
	void setDay(Day value);

	/**
	 * Returns the value of the '<em><b>Time From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time From</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time From</em>' reference.
	 * @see #setTimeFrom(Time)
	 * @see course.CoursePackage#getTimetable_TimeFrom()
	 * @model
	 * @generated
	 */
	Time getTimeFrom();

	/**
	 * Sets the value of the '{@link course.Timetable#getTimeFrom <em>Time From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time From</em>' reference.
	 * @see #getTimeFrom()
	 * @generated
	 */
	void setTimeFrom(Time value);

	/**
	 * Returns the value of the '<em><b>Time To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time To</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time To</em>' reference.
	 * @see #setTimeTo(Time)
	 * @see course.CoursePackage#getTimetable_TimeTo()
	 * @model
	 * @generated
	 */
	Time getTimeTo();

	/**
	 * Sets the value of the '{@link course.Timetable#getTimeTo <em>Time To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time To</em>' reference.
	 * @see #getTimeTo()
	 * @generated
	 */
	void setTimeTo(Time value);

	/**
	 * Returns the value of the '<em><b>Week From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Week From</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Week From</em>' attribute.
	 * @see #setWeekFrom(int)
	 * @see course.CoursePackage#getTimetable_WeekFrom()
	 * @model
	 * @generated
	 */
	int getWeekFrom();

	/**
	 * Sets the value of the '{@link course.Timetable#getWeekFrom <em>Week From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Week From</em>' attribute.
	 * @see #getWeekFrom()
	 * @generated
	 */
	void setWeekFrom(int value);

	/**
	 * Returns the value of the '<em><b>Week To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Week To</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Week To</em>' attribute.
	 * @see #setWeekTo(int)
	 * @see course.CoursePackage#getTimetable_WeekTo()
	 * @model
	 * @generated
	 */
	int getWeekTo();

	/**
	 * Sets the value of the '{@link course.Timetable#getWeekTo <em>Week To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Week To</em>' attribute.
	 * @see #getWeekTo()
	 * @generated
	 */
	void setWeekTo(int value);

	/**
	 * Returns the value of the '<em><b>Activity</b></em>' attribute.
	 * The literals are from the enumeration {@link course.Activity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity</em>' attribute.
	 * @see course.Activity
	 * @see #setActivity(Activity)
	 * @see course.CoursePackage#getTimetable_Activity()
	 * @model
	 * @generated
	 */
	Activity getActivity();

	/**
	 * Sets the value of the '{@link course.Timetable#getActivity <em>Activity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Activity</em>' attribute.
	 * @see course.Activity
	 * @see #getActivity()
	 * @generated
	 */
	void setActivity(Activity value);

	/**
	 * Returns the value of the '<em><b>Planned For</b></em>' reference list.
	 * The list contents are of type {@link course.Study}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Planned For</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Planned For</em>' reference list.
	 * @see course.CoursePackage#getTimetable_PlannedFor()
	 * @model
	 * @generated
	 */
	EList<Study> getPlannedFor();

	/**
	 * Returns the value of the '<em><b>Room</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room</em>' reference.
	 * @see #setRoom(Room)
	 * @see course.CoursePackage#getTimetable_Room()
	 * @model
	 * @generated
	 */
	Room getRoom();

	/**
	 * Sets the value of the '{@link course.Timetable#getRoom <em>Room</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room</em>' reference.
	 * @see #getRoom()
	 * @generated
	 */
	void setRoom(Room value);

} // Timetable
