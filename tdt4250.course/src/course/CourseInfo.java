/**
 */
package course;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link course.CourseInfo#getContent <em>Content</em>}</li>
 *   <li>{@link course.CourseInfo#getLearningOutcome <em>Learning Outcome</em>}</li>
 *   <li>{@link course.CourseInfo#getLearningMethodsAndActivities <em>Learning Methods And Activities</em>}</li>
 *   <li>{@link course.CourseInfo#getCompulsoryAssignments <em>Compulsory Assignments</em>}</li>
 *   <li>{@link course.CourseInfo#getFurtherOnEvaluation <em>Further On Evaluation</em>}</li>
 *   <li>{@link course.CourseInfo#getSpecificConditions <em>Specific Conditions</em>}</li>
 *   <li>{@link course.CourseInfo#getRecommendedPreviousKnowledge <em>Recommended Previous Knowledge</em>}</li>
 *   <li>{@link course.CourseInfo#getCourseMaterials <em>Course Materials</em>}</li>
 * </ul>
 *
 * @see course.CoursePackage#getCourseInfo()
 * @model
 * @generated
 */
public interface CourseInfo extends EObject {
	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(String)
	 * @see course.CoursePackage#getCourseInfo_Content()
	 * @model
	 * @generated
	 */
	String getContent();

	/**
	 * Sets the value of the '{@link course.CourseInfo#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(String value);

	/**
	 * Returns the value of the '<em><b>Learning Outcome</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Learning Outcome</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Learning Outcome</em>' attribute.
	 * @see #setLearningOutcome(String)
	 * @see course.CoursePackage#getCourseInfo_LearningOutcome()
	 * @model
	 * @generated
	 */
	String getLearningOutcome();

	/**
	 * Sets the value of the '{@link course.CourseInfo#getLearningOutcome <em>Learning Outcome</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Learning Outcome</em>' attribute.
	 * @see #getLearningOutcome()
	 * @generated
	 */
	void setLearningOutcome(String value);

	/**
	 * Returns the value of the '<em><b>Learning Methods And Activities</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Learning Methods And Activities</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Learning Methods And Activities</em>' attribute.
	 * @see #setLearningMethodsAndActivities(String)
	 * @see course.CoursePackage#getCourseInfo_LearningMethodsAndActivities()
	 * @model
	 * @generated
	 */
	String getLearningMethodsAndActivities();

	/**
	 * Sets the value of the '{@link course.CourseInfo#getLearningMethodsAndActivities <em>Learning Methods And Activities</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Learning Methods And Activities</em>' attribute.
	 * @see #getLearningMethodsAndActivities()
	 * @generated
	 */
	void setLearningMethodsAndActivities(String value);

	/**
	 * Returns the value of the '<em><b>Compulsory Assignments</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compulsory Assignments</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compulsory Assignments</em>' attribute.
	 * @see #setCompulsoryAssignments(String)
	 * @see course.CoursePackage#getCourseInfo_CompulsoryAssignments()
	 * @model
	 * @generated
	 */
	String getCompulsoryAssignments();

	/**
	 * Sets the value of the '{@link course.CourseInfo#getCompulsoryAssignments <em>Compulsory Assignments</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Compulsory Assignments</em>' attribute.
	 * @see #getCompulsoryAssignments()
	 * @generated
	 */
	void setCompulsoryAssignments(String value);

	/**
	 * Returns the value of the '<em><b>Further On Evaluation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Further On Evaluation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Further On Evaluation</em>' attribute.
	 * @see #setFurtherOnEvaluation(String)
	 * @see course.CoursePackage#getCourseInfo_FurtherOnEvaluation()
	 * @model
	 * @generated
	 */
	String getFurtherOnEvaluation();

	/**
	 * Sets the value of the '{@link course.CourseInfo#getFurtherOnEvaluation <em>Further On Evaluation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Further On Evaluation</em>' attribute.
	 * @see #getFurtherOnEvaluation()
	 * @generated
	 */
	void setFurtherOnEvaluation(String value);

	/**
	 * Returns the value of the '<em><b>Specific Conditions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specific Conditions</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specific Conditions</em>' attribute.
	 * @see #setSpecificConditions(String)
	 * @see course.CoursePackage#getCourseInfo_SpecificConditions()
	 * @model
	 * @generated
	 */
	String getSpecificConditions();

	/**
	 * Sets the value of the '{@link course.CourseInfo#getSpecificConditions <em>Specific Conditions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specific Conditions</em>' attribute.
	 * @see #getSpecificConditions()
	 * @generated
	 */
	void setSpecificConditions(String value);

	/**
	 * Returns the value of the '<em><b>Recommended Previous Knowledge</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Recommended Previous Knowledge</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Recommended Previous Knowledge</em>' attribute.
	 * @see #setRecommendedPreviousKnowledge(String)
	 * @see course.CoursePackage#getCourseInfo_RecommendedPreviousKnowledge()
	 * @model
	 * @generated
	 */
	String getRecommendedPreviousKnowledge();

	/**
	 * Sets the value of the '{@link course.CourseInfo#getRecommendedPreviousKnowledge <em>Recommended Previous Knowledge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Recommended Previous Knowledge</em>' attribute.
	 * @see #getRecommendedPreviousKnowledge()
	 * @generated
	 */
	void setRecommendedPreviousKnowledge(String value);

	/**
	 * Returns the value of the '<em><b>Course Materials</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Materials</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Materials</em>' attribute.
	 * @see #setCourseMaterials(String)
	 * @see course.CoursePackage#getCourseInfo_CourseMaterials()
	 * @model
	 * @generated
	 */
	String getCourseMaterials();

	/**
	 * Sets the value of the '{@link course.CourseInfo#getCourseMaterials <em>Course Materials</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Materials</em>' attribute.
	 * @see #getCourseMaterials()
	 * @generated
	 */
	void setCourseMaterials(String value);

} // CourseInfo
