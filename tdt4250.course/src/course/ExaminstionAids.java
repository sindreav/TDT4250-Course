/**
 */
package course;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Examinstion Aids</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link course.ExaminstionAids#getChar <em>Char</em>}</li>
 *   <li>{@link course.ExaminstionAids#getInfoText <em>Info Text</em>}</li>
 *   <li>{@link course.ExaminstionAids#getWebsite <em>Website</em>}</li>
 * </ul>
 *
 * @see course.CoursePackage#getExaminstionAids()
 * @model
 * @generated
 */
public interface ExaminstionAids extends EObject {
	/**
	 * Returns the value of the '<em><b>Char</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Char</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Char</em>' attribute.
	 * @see #setChar(String)
	 * @see course.CoursePackage#getExaminstionAids_Char()
	 * @model
	 * @generated
	 */
	String getChar();

	/**
	 * Sets the value of the '{@link course.ExaminstionAids#getChar <em>Char</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Char</em>' attribute.
	 * @see #getChar()
	 * @generated
	 */
	void setChar(String value);

	/**
	 * Returns the value of the '<em><b>Info Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Info Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Info Text</em>' attribute.
	 * @see #setInfoText(String)
	 * @see course.CoursePackage#getExaminstionAids_InfoText()
	 * @model
	 * @generated
	 */
	String getInfoText();

	/**
	 * Sets the value of the '{@link course.ExaminstionAids#getInfoText <em>Info Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Info Text</em>' attribute.
	 * @see #getInfoText()
	 * @generated
	 */
	void setInfoText(String value);

	/**
	 * Returns the value of the '<em><b>Website</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Website</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Website</em>' attribute.
	 * @see #setWebsite(String)
	 * @see course.CoursePackage#getExaminstionAids_Website()
	 * @model
	 * @generated
	 */
	String getWebsite();

	/**
	 * Sets the value of the '{@link course.ExaminstionAids#getWebsite <em>Website</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Website</em>' attribute.
	 * @see #getWebsite()
	 * @generated
	 */
	void setWebsite(String value);

} // ExaminstionAids
