/**
 */
package course.impl;

import course.CourseInfo;
import course.CoursePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link course.impl.CourseInfoImpl#getContent <em>Content</em>}</li>
 *   <li>{@link course.impl.CourseInfoImpl#getLearningOutcome <em>Learning Outcome</em>}</li>
 *   <li>{@link course.impl.CourseInfoImpl#getLearningMethodsAndActivities <em>Learning Methods And Activities</em>}</li>
 *   <li>{@link course.impl.CourseInfoImpl#getCompulsoryAssignments <em>Compulsory Assignments</em>}</li>
 *   <li>{@link course.impl.CourseInfoImpl#getFurtherOnEvaluation <em>Further On Evaluation</em>}</li>
 *   <li>{@link course.impl.CourseInfoImpl#getSpecificConditions <em>Specific Conditions</em>}</li>
 *   <li>{@link course.impl.CourseInfoImpl#getRecommendedPreviousKnowledge <em>Recommended Previous Knowledge</em>}</li>
 *   <li>{@link course.impl.CourseInfoImpl#getCourseMaterials <em>Course Materials</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseInfoImpl extends MinimalEObjectImpl.Container implements CourseInfo {
	/**
	 * The default value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected String content = CONTENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getLearningOutcome() <em>Learning Outcome</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLearningOutcome()
	 * @generated
	 * @ordered
	 */
	protected static final String LEARNING_OUTCOME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLearningOutcome() <em>Learning Outcome</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLearningOutcome()
	 * @generated
	 * @ordered
	 */
	protected String learningOutcome = LEARNING_OUTCOME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLearningMethodsAndActivities() <em>Learning Methods And Activities</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLearningMethodsAndActivities()
	 * @generated
	 * @ordered
	 */
	protected static final String LEARNING_METHODS_AND_ACTIVITIES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLearningMethodsAndActivities() <em>Learning Methods And Activities</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLearningMethodsAndActivities()
	 * @generated
	 * @ordered
	 */
	protected String learningMethodsAndActivities = LEARNING_METHODS_AND_ACTIVITIES_EDEFAULT;

	/**
	 * The default value of the '{@link #getCompulsoryAssignments() <em>Compulsory Assignments</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompulsoryAssignments()
	 * @generated
	 * @ordered
	 */
	protected static final String COMPULSORY_ASSIGNMENTS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCompulsoryAssignments() <em>Compulsory Assignments</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompulsoryAssignments()
	 * @generated
	 * @ordered
	 */
	protected String compulsoryAssignments = COMPULSORY_ASSIGNMENTS_EDEFAULT;

	/**
	 * The default value of the '{@link #getFurtherOnEvaluation() <em>Further On Evaluation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFurtherOnEvaluation()
	 * @generated
	 * @ordered
	 */
	protected static final String FURTHER_ON_EVALUATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFurtherOnEvaluation() <em>Further On Evaluation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFurtherOnEvaluation()
	 * @generated
	 * @ordered
	 */
	protected String furtherOnEvaluation = FURTHER_ON_EVALUATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getSpecificConditions() <em>Specific Conditions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecificConditions()
	 * @generated
	 * @ordered
	 */
	protected static final String SPECIFIC_CONDITIONS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSpecificConditions() <em>Specific Conditions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecificConditions()
	 * @generated
	 * @ordered
	 */
	protected String specificConditions = SPECIFIC_CONDITIONS_EDEFAULT;

	/**
	 * The default value of the '{@link #getRecommendedPreviousKnowledge() <em>Recommended Previous Knowledge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecommendedPreviousKnowledge()
	 * @generated
	 * @ordered
	 */
	protected static final String RECOMMENDED_PREVIOUS_KNOWLEDGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRecommendedPreviousKnowledge() <em>Recommended Previous Knowledge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecommendedPreviousKnowledge()
	 * @generated
	 * @ordered
	 */
	protected String recommendedPreviousKnowledge = RECOMMENDED_PREVIOUS_KNOWLEDGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCourseMaterials() <em>Course Materials</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseMaterials()
	 * @generated
	 * @ordered
	 */
	protected static final String COURSE_MATERIALS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCourseMaterials() <em>Course Materials</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseMaterials()
	 * @generated
	 * @ordered
	 */
	protected String courseMaterials = COURSE_MATERIALS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseInfoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePackage.Literals.COURSE_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContent() {
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(String newContent) {
		String oldContent = content;
		content = newContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INFO__CONTENT, oldContent, content));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLearningOutcome() {
		return learningOutcome;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLearningOutcome(String newLearningOutcome) {
		String oldLearningOutcome = learningOutcome;
		learningOutcome = newLearningOutcome;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INFO__LEARNING_OUTCOME, oldLearningOutcome, learningOutcome));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLearningMethodsAndActivities() {
		return learningMethodsAndActivities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLearningMethodsAndActivities(String newLearningMethodsAndActivities) {
		String oldLearningMethodsAndActivities = learningMethodsAndActivities;
		learningMethodsAndActivities = newLearningMethodsAndActivities;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INFO__LEARNING_METHODS_AND_ACTIVITIES, oldLearningMethodsAndActivities, learningMethodsAndActivities));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCompulsoryAssignments() {
		return compulsoryAssignments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCompulsoryAssignments(String newCompulsoryAssignments) {
		String oldCompulsoryAssignments = compulsoryAssignments;
		compulsoryAssignments = newCompulsoryAssignments;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INFO__COMPULSORY_ASSIGNMENTS, oldCompulsoryAssignments, compulsoryAssignments));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFurtherOnEvaluation() {
		return furtherOnEvaluation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFurtherOnEvaluation(String newFurtherOnEvaluation) {
		String oldFurtherOnEvaluation = furtherOnEvaluation;
		furtherOnEvaluation = newFurtherOnEvaluation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INFO__FURTHER_ON_EVALUATION, oldFurtherOnEvaluation, furtherOnEvaluation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSpecificConditions() {
		return specificConditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecificConditions(String newSpecificConditions) {
		String oldSpecificConditions = specificConditions;
		specificConditions = newSpecificConditions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INFO__SPECIFIC_CONDITIONS, oldSpecificConditions, specificConditions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRecommendedPreviousKnowledge() {
		return recommendedPreviousKnowledge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecommendedPreviousKnowledge(String newRecommendedPreviousKnowledge) {
		String oldRecommendedPreviousKnowledge = recommendedPreviousKnowledge;
		recommendedPreviousKnowledge = newRecommendedPreviousKnowledge;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INFO__RECOMMENDED_PREVIOUS_KNOWLEDGE, oldRecommendedPreviousKnowledge, recommendedPreviousKnowledge));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCourseMaterials() {
		return courseMaterials;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseMaterials(String newCourseMaterials) {
		String oldCourseMaterials = courseMaterials;
		courseMaterials = newCourseMaterials;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INFO__COURSE_MATERIALS, oldCourseMaterials, courseMaterials));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursePackage.COURSE_INFO__CONTENT:
				return getContent();
			case CoursePackage.COURSE_INFO__LEARNING_OUTCOME:
				return getLearningOutcome();
			case CoursePackage.COURSE_INFO__LEARNING_METHODS_AND_ACTIVITIES:
				return getLearningMethodsAndActivities();
			case CoursePackage.COURSE_INFO__COMPULSORY_ASSIGNMENTS:
				return getCompulsoryAssignments();
			case CoursePackage.COURSE_INFO__FURTHER_ON_EVALUATION:
				return getFurtherOnEvaluation();
			case CoursePackage.COURSE_INFO__SPECIFIC_CONDITIONS:
				return getSpecificConditions();
			case CoursePackage.COURSE_INFO__RECOMMENDED_PREVIOUS_KNOWLEDGE:
				return getRecommendedPreviousKnowledge();
			case CoursePackage.COURSE_INFO__COURSE_MATERIALS:
				return getCourseMaterials();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursePackage.COURSE_INFO__CONTENT:
				setContent((String)newValue);
				return;
			case CoursePackage.COURSE_INFO__LEARNING_OUTCOME:
				setLearningOutcome((String)newValue);
				return;
			case CoursePackage.COURSE_INFO__LEARNING_METHODS_AND_ACTIVITIES:
				setLearningMethodsAndActivities((String)newValue);
				return;
			case CoursePackage.COURSE_INFO__COMPULSORY_ASSIGNMENTS:
				setCompulsoryAssignments((String)newValue);
				return;
			case CoursePackage.COURSE_INFO__FURTHER_ON_EVALUATION:
				setFurtherOnEvaluation((String)newValue);
				return;
			case CoursePackage.COURSE_INFO__SPECIFIC_CONDITIONS:
				setSpecificConditions((String)newValue);
				return;
			case CoursePackage.COURSE_INFO__RECOMMENDED_PREVIOUS_KNOWLEDGE:
				setRecommendedPreviousKnowledge((String)newValue);
				return;
			case CoursePackage.COURSE_INFO__COURSE_MATERIALS:
				setCourseMaterials((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursePackage.COURSE_INFO__CONTENT:
				setContent(CONTENT_EDEFAULT);
				return;
			case CoursePackage.COURSE_INFO__LEARNING_OUTCOME:
				setLearningOutcome(LEARNING_OUTCOME_EDEFAULT);
				return;
			case CoursePackage.COURSE_INFO__LEARNING_METHODS_AND_ACTIVITIES:
				setLearningMethodsAndActivities(LEARNING_METHODS_AND_ACTIVITIES_EDEFAULT);
				return;
			case CoursePackage.COURSE_INFO__COMPULSORY_ASSIGNMENTS:
				setCompulsoryAssignments(COMPULSORY_ASSIGNMENTS_EDEFAULT);
				return;
			case CoursePackage.COURSE_INFO__FURTHER_ON_EVALUATION:
				setFurtherOnEvaluation(FURTHER_ON_EVALUATION_EDEFAULT);
				return;
			case CoursePackage.COURSE_INFO__SPECIFIC_CONDITIONS:
				setSpecificConditions(SPECIFIC_CONDITIONS_EDEFAULT);
				return;
			case CoursePackage.COURSE_INFO__RECOMMENDED_PREVIOUS_KNOWLEDGE:
				setRecommendedPreviousKnowledge(RECOMMENDED_PREVIOUS_KNOWLEDGE_EDEFAULT);
				return;
			case CoursePackage.COURSE_INFO__COURSE_MATERIALS:
				setCourseMaterials(COURSE_MATERIALS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursePackage.COURSE_INFO__CONTENT:
				return CONTENT_EDEFAULT == null ? content != null : !CONTENT_EDEFAULT.equals(content);
			case CoursePackage.COURSE_INFO__LEARNING_OUTCOME:
				return LEARNING_OUTCOME_EDEFAULT == null ? learningOutcome != null : !LEARNING_OUTCOME_EDEFAULT.equals(learningOutcome);
			case CoursePackage.COURSE_INFO__LEARNING_METHODS_AND_ACTIVITIES:
				return LEARNING_METHODS_AND_ACTIVITIES_EDEFAULT == null ? learningMethodsAndActivities != null : !LEARNING_METHODS_AND_ACTIVITIES_EDEFAULT.equals(learningMethodsAndActivities);
			case CoursePackage.COURSE_INFO__COMPULSORY_ASSIGNMENTS:
				return COMPULSORY_ASSIGNMENTS_EDEFAULT == null ? compulsoryAssignments != null : !COMPULSORY_ASSIGNMENTS_EDEFAULT.equals(compulsoryAssignments);
			case CoursePackage.COURSE_INFO__FURTHER_ON_EVALUATION:
				return FURTHER_ON_EVALUATION_EDEFAULT == null ? furtherOnEvaluation != null : !FURTHER_ON_EVALUATION_EDEFAULT.equals(furtherOnEvaluation);
			case CoursePackage.COURSE_INFO__SPECIFIC_CONDITIONS:
				return SPECIFIC_CONDITIONS_EDEFAULT == null ? specificConditions != null : !SPECIFIC_CONDITIONS_EDEFAULT.equals(specificConditions);
			case CoursePackage.COURSE_INFO__RECOMMENDED_PREVIOUS_KNOWLEDGE:
				return RECOMMENDED_PREVIOUS_KNOWLEDGE_EDEFAULT == null ? recommendedPreviousKnowledge != null : !RECOMMENDED_PREVIOUS_KNOWLEDGE_EDEFAULT.equals(recommendedPreviousKnowledge);
			case CoursePackage.COURSE_INFO__COURSE_MATERIALS:
				return COURSE_MATERIALS_EDEFAULT == null ? courseMaterials != null : !COURSE_MATERIALS_EDEFAULT.equals(courseMaterials);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (content: ");
		result.append(content);
		result.append(", learningOutcome: ");
		result.append(learningOutcome);
		result.append(", learningMethodsAndActivities: ");
		result.append(learningMethodsAndActivities);
		result.append(", compulsoryAssignments: ");
		result.append(compulsoryAssignments);
		result.append(", furtherOnEvaluation: ");
		result.append(furtherOnEvaluation);
		result.append(", specificConditions: ");
		result.append(specificConditions);
		result.append(", recommendedPreviousKnowledge: ");
		result.append(recommendedPreviousKnowledge);
		result.append(", courseMaterials: ");
		result.append(courseMaterials);
		result.append(')');
		return result.toString();
	}

} //CourseInfoImpl
