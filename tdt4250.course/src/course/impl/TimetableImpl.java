/**
 */
package course.impl;

import course.Activity;
import course.CoursePackage;
import course.Day;
import course.Room;
import course.Study;
import course.Time;
import course.Timetable;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timetable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link course.impl.TimetableImpl#getDay <em>Day</em>}</li>
 *   <li>{@link course.impl.TimetableImpl#getTimeFrom <em>Time From</em>}</li>
 *   <li>{@link course.impl.TimetableImpl#getTimeTo <em>Time To</em>}</li>
 *   <li>{@link course.impl.TimetableImpl#getWeekFrom <em>Week From</em>}</li>
 *   <li>{@link course.impl.TimetableImpl#getWeekTo <em>Week To</em>}</li>
 *   <li>{@link course.impl.TimetableImpl#getActivity <em>Activity</em>}</li>
 *   <li>{@link course.impl.TimetableImpl#getPlannedFor <em>Planned For</em>}</li>
 *   <li>{@link course.impl.TimetableImpl#getRoom <em>Room</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimetableImpl extends MinimalEObjectImpl.Container implements Timetable {
	/**
	 * The default value of the '{@link #getDay() <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDay()
	 * @generated
	 * @ordered
	 */
	protected static final Day DAY_EDEFAULT = Day.MONDAY;

	/**
	 * The cached value of the '{@link #getDay() <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDay()
	 * @generated
	 * @ordered
	 */
	protected Day day = DAY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTimeFrom() <em>Time From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeFrom()
	 * @generated
	 * @ordered
	 */
	protected Time timeFrom;

	/**
	 * The cached value of the '{@link #getTimeTo() <em>Time To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeTo()
	 * @generated
	 * @ordered
	 */
	protected Time timeTo;

	/**
	 * The default value of the '{@link #getWeekFrom() <em>Week From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeekFrom()
	 * @generated
	 * @ordered
	 */
	protected static final int WEEK_FROM_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getWeekFrom() <em>Week From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeekFrom()
	 * @generated
	 * @ordered
	 */
	protected int weekFrom = WEEK_FROM_EDEFAULT;

	/**
	 * The default value of the '{@link #getWeekTo() <em>Week To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeekTo()
	 * @generated
	 * @ordered
	 */
	protected static final int WEEK_TO_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getWeekTo() <em>Week To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeekTo()
	 * @generated
	 * @ordered
	 */
	protected int weekTo = WEEK_TO_EDEFAULT;

	/**
	 * The default value of the '{@link #getActivity() <em>Activity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivity()
	 * @generated
	 * @ordered
	 */
	protected static final Activity ACTIVITY_EDEFAULT = Activity.�VING;

	/**
	 * The cached value of the '{@link #getActivity() <em>Activity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivity()
	 * @generated
	 * @ordered
	 */
	protected Activity activity = ACTIVITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPlannedFor() <em>Planned For</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlannedFor()
	 * @generated
	 * @ordered
	 */
	protected EList<Study> plannedFor;

	/**
	 * The cached value of the '{@link #getRoom() <em>Room</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected Room room;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimetableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePackage.Literals.TIMETABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Day getDay() {
		return day;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDay(Day newDay) {
		Day oldDay = day;
		day = newDay == null ? DAY_EDEFAULT : newDay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.TIMETABLE__DAY, oldDay, day));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Time getTimeFrom() {
		if (timeFrom != null && timeFrom.eIsProxy()) {
			InternalEObject oldTimeFrom = (InternalEObject)timeFrom;
			timeFrom = (Time)eResolveProxy(oldTimeFrom);
			if (timeFrom != oldTimeFrom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoursePackage.TIMETABLE__TIME_FROM, oldTimeFrom, timeFrom));
			}
		}
		return timeFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Time basicGetTimeFrom() {
		return timeFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeFrom(Time newTimeFrom) {
		Time oldTimeFrom = timeFrom;
		timeFrom = newTimeFrom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.TIMETABLE__TIME_FROM, oldTimeFrom, timeFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Time getTimeTo() {
		if (timeTo != null && timeTo.eIsProxy()) {
			InternalEObject oldTimeTo = (InternalEObject)timeTo;
			timeTo = (Time)eResolveProxy(oldTimeTo);
			if (timeTo != oldTimeTo) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoursePackage.TIMETABLE__TIME_TO, oldTimeTo, timeTo));
			}
		}
		return timeTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Time basicGetTimeTo() {
		return timeTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeTo(Time newTimeTo) {
		Time oldTimeTo = timeTo;
		timeTo = newTimeTo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.TIMETABLE__TIME_TO, oldTimeTo, timeTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getWeekFrom() {
		return weekFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWeekFrom(int newWeekFrom) {
		int oldWeekFrom = weekFrom;
		weekFrom = newWeekFrom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.TIMETABLE__WEEK_FROM, oldWeekFrom, weekFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getWeekTo() {
		return weekTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWeekTo(int newWeekTo) {
		int oldWeekTo = weekTo;
		weekTo = newWeekTo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.TIMETABLE__WEEK_TO, oldWeekTo, weekTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Activity getActivity() {
		return activity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActivity(Activity newActivity) {
		Activity oldActivity = activity;
		activity = newActivity == null ? ACTIVITY_EDEFAULT : newActivity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.TIMETABLE__ACTIVITY, oldActivity, activity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Study> getPlannedFor() {
		if (plannedFor == null) {
			plannedFor = new EObjectResolvingEList<Study>(Study.class, this, CoursePackage.TIMETABLE__PLANNED_FOR);
		}
		return plannedFor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room getRoom() {
		if (room != null && room.eIsProxy()) {
			InternalEObject oldRoom = (InternalEObject)room;
			room = (Room)eResolveProxy(oldRoom);
			if (room != oldRoom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoursePackage.TIMETABLE__ROOM, oldRoom, room));
			}
		}
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room basicGetRoom() {
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoom(Room newRoom) {
		Room oldRoom = room;
		room = newRoom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.TIMETABLE__ROOM, oldRoom, room));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursePackage.TIMETABLE__DAY:
				return getDay();
			case CoursePackage.TIMETABLE__TIME_FROM:
				if (resolve) return getTimeFrom();
				return basicGetTimeFrom();
			case CoursePackage.TIMETABLE__TIME_TO:
				if (resolve) return getTimeTo();
				return basicGetTimeTo();
			case CoursePackage.TIMETABLE__WEEK_FROM:
				return getWeekFrom();
			case CoursePackage.TIMETABLE__WEEK_TO:
				return getWeekTo();
			case CoursePackage.TIMETABLE__ACTIVITY:
				return getActivity();
			case CoursePackage.TIMETABLE__PLANNED_FOR:
				return getPlannedFor();
			case CoursePackage.TIMETABLE__ROOM:
				if (resolve) return getRoom();
				return basicGetRoom();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursePackage.TIMETABLE__DAY:
				setDay((Day)newValue);
				return;
			case CoursePackage.TIMETABLE__TIME_FROM:
				setTimeFrom((Time)newValue);
				return;
			case CoursePackage.TIMETABLE__TIME_TO:
				setTimeTo((Time)newValue);
				return;
			case CoursePackage.TIMETABLE__WEEK_FROM:
				setWeekFrom((Integer)newValue);
				return;
			case CoursePackage.TIMETABLE__WEEK_TO:
				setWeekTo((Integer)newValue);
				return;
			case CoursePackage.TIMETABLE__ACTIVITY:
				setActivity((Activity)newValue);
				return;
			case CoursePackage.TIMETABLE__PLANNED_FOR:
				getPlannedFor().clear();
				getPlannedFor().addAll((Collection<? extends Study>)newValue);
				return;
			case CoursePackage.TIMETABLE__ROOM:
				setRoom((Room)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursePackage.TIMETABLE__DAY:
				setDay(DAY_EDEFAULT);
				return;
			case CoursePackage.TIMETABLE__TIME_FROM:
				setTimeFrom((Time)null);
				return;
			case CoursePackage.TIMETABLE__TIME_TO:
				setTimeTo((Time)null);
				return;
			case CoursePackage.TIMETABLE__WEEK_FROM:
				setWeekFrom(WEEK_FROM_EDEFAULT);
				return;
			case CoursePackage.TIMETABLE__WEEK_TO:
				setWeekTo(WEEK_TO_EDEFAULT);
				return;
			case CoursePackage.TIMETABLE__ACTIVITY:
				setActivity(ACTIVITY_EDEFAULT);
				return;
			case CoursePackage.TIMETABLE__PLANNED_FOR:
				getPlannedFor().clear();
				return;
			case CoursePackage.TIMETABLE__ROOM:
				setRoom((Room)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursePackage.TIMETABLE__DAY:
				return day != DAY_EDEFAULT;
			case CoursePackage.TIMETABLE__TIME_FROM:
				return timeFrom != null;
			case CoursePackage.TIMETABLE__TIME_TO:
				return timeTo != null;
			case CoursePackage.TIMETABLE__WEEK_FROM:
				return weekFrom != WEEK_FROM_EDEFAULT;
			case CoursePackage.TIMETABLE__WEEK_TO:
				return weekTo != WEEK_TO_EDEFAULT;
			case CoursePackage.TIMETABLE__ACTIVITY:
				return activity != ACTIVITY_EDEFAULT;
			case CoursePackage.TIMETABLE__PLANNED_FOR:
				return plannedFor != null && !plannedFor.isEmpty();
			case CoursePackage.TIMETABLE__ROOM:
				return room != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (day: ");
		result.append(day);
		result.append(", weekFrom: ");
		result.append(weekFrom);
		result.append(", weekTo: ");
		result.append(weekTo);
		result.append(", activity: ");
		result.append(activity);
		result.append(')');
		return result.toString();
	}

} //TimetableImpl
