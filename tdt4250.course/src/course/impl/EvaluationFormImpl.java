/**
 */
package course.impl;

import course.CoursePackage;
import course.EvaluationForm;

import course.EvaluationType;
import course.ExaminationAid;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Evaluation Form</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link course.impl.EvaluationFormImpl#getType <em>Type</em>}</li>
 *   <li>{@link course.impl.EvaluationFormImpl#getWeight <em>Weight</em>}</li>
 *   <li>{@link course.impl.EvaluationFormImpl#getDuration <em>Duration</em>}</li>
 *   <li>{@link course.impl.EvaluationFormImpl#getExaminationAid <em>Examination Aid</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EvaluationFormImpl extends MinimalEObjectImpl.Container implements EvaluationForm {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final EvaluationType TYPE_EDEFAULT = EvaluationType.WRITTEN_EXAMINATION;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected EvaluationType type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getWeight() <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeight()
	 * @generated
	 * @ordered
	 */
	protected static final double WEIGHT_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getWeight() <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeight()
	 * @generated
	 * @ordered
	 */
	protected double weight = WEIGHT_EDEFAULT;

	/**
	 * The default value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected static final double DURATION_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected double duration = DURATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getExaminationAid() <em>Examination Aid</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExaminationAid()
	 * @generated
	 * @ordered
	 */
	protected ExaminationAid examinationAid;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EvaluationFormImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePackage.Literals.EVALUATION_FORM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluationType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(EvaluationType newType) {
		EvaluationType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.EVALUATION_FORM__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWeight(double newWeight) {
		double oldWeight = weight;
		weight = newWeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.EVALUATION_FORM__WEIGHT, oldWeight, weight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getDuration() {
		return duration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDuration(double newDuration) {
		double oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.EVALUATION_FORM__DURATION, oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExaminationAid getExaminationAid() {
		if (examinationAid != null && examinationAid.eIsProxy()) {
			InternalEObject oldExaminationAid = (InternalEObject)examinationAid;
			examinationAid = (ExaminationAid)eResolveProxy(oldExaminationAid);
			if (examinationAid != oldExaminationAid) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoursePackage.EVALUATION_FORM__EXAMINATION_AID, oldExaminationAid, examinationAid));
			}
		}
		return examinationAid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExaminationAid basicGetExaminationAid() {
		return examinationAid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExaminationAid(ExaminationAid newExaminationAid) {
		ExaminationAid oldExaminationAid = examinationAid;
		examinationAid = newExaminationAid;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.EVALUATION_FORM__EXAMINATION_AID, oldExaminationAid, examinationAid));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursePackage.EVALUATION_FORM__TYPE:
				return getType();
			case CoursePackage.EVALUATION_FORM__WEIGHT:
				return getWeight();
			case CoursePackage.EVALUATION_FORM__DURATION:
				return getDuration();
			case CoursePackage.EVALUATION_FORM__EXAMINATION_AID:
				if (resolve) return getExaminationAid();
				return basicGetExaminationAid();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursePackage.EVALUATION_FORM__TYPE:
				setType((EvaluationType)newValue);
				return;
			case CoursePackage.EVALUATION_FORM__WEIGHT:
				setWeight((Double)newValue);
				return;
			case CoursePackage.EVALUATION_FORM__DURATION:
				setDuration((Double)newValue);
				return;
			case CoursePackage.EVALUATION_FORM__EXAMINATION_AID:
				setExaminationAid((ExaminationAid)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursePackage.EVALUATION_FORM__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case CoursePackage.EVALUATION_FORM__WEIGHT:
				setWeight(WEIGHT_EDEFAULT);
				return;
			case CoursePackage.EVALUATION_FORM__DURATION:
				setDuration(DURATION_EDEFAULT);
				return;
			case CoursePackage.EVALUATION_FORM__EXAMINATION_AID:
				setExaminationAid((ExaminationAid)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursePackage.EVALUATION_FORM__TYPE:
				return type != TYPE_EDEFAULT;
			case CoursePackage.EVALUATION_FORM__WEIGHT:
				return weight != WEIGHT_EDEFAULT;
			case CoursePackage.EVALUATION_FORM__DURATION:
				return duration != DURATION_EDEFAULT;
			case CoursePackage.EVALUATION_FORM__EXAMINATION_AID:
				return examinationAid != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(", weight: ");
		result.append(weight);
		result.append(", duration: ");
		result.append(duration);
		result.append(')');
		return result.toString();
	}

} //EvaluationFormImpl
