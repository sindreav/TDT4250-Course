/**
 */
package course.impl;

import course.Course;
import course.CourseInfo;
import course.CourseInstance;
import course.CoursePackage;
import course.CreditReduction;

import course.Department;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link course.impl.CourseImpl#getCode <em>Code</em>}</li>
 *   <li>{@link course.impl.CourseImpl#getName <em>Name</em>}</li>
 *   <li>{@link course.impl.CourseImpl#getCredits <em>Credits</em>}</li>
 *   <li>{@link course.impl.CourseImpl#getLevel <em>Level</em>}</li>
 *   <li>{@link course.impl.CourseImpl#getCourseInfo <em>Course Info</em>}</li>
 *   <li>{@link course.impl.CourseImpl#getDepartment <em>Department</em>}</li>
 *   <li>{@link course.impl.CourseImpl#getCourseInstances <em>Course Instances</em>}</li>
 *   <li>{@link course.impl.CourseImpl#getCreditReductions <em>Credit Reductions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseImpl extends MinimalEObjectImpl.Container implements Course {
	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected String code = CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected static final double CREDITS_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected double credits = CREDITS_EDEFAULT;

	/**
	 * The default value of the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected static final String LEVEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected String level = LEVEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCourseInfo() <em>Course Info</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseInfo()
	 * @generated
	 * @ordered
	 */
	protected CourseInfo courseInfo;

	/**
	 * The cached value of the '{@link #getDepartment() <em>Department</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepartment()
	 * @generated
	 * @ordered
	 */
	protected Department department;

	/**
	 * The cached value of the '{@link #getCourseInstances() <em>Course Instances</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseInstance> courseInstances;

	/**
	 * The cached value of the '{@link #getCreditReductions() <em>Credit Reductions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreditReductions()
	 * @generated
	 * @ordered
	 */
	protected EList<CreditReduction> creditReductions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePackage.Literals.COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(String newCode) {
		String oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCredits() {
		return credits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCredits(double newCredits) {
		double oldCredits = credits;
		credits = newCredits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE__CREDITS, oldCredits, credits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLevel(String newLevel) {
		String oldLevel = level;
		level = newLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE__LEVEL, oldLevel, level));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseInfo getCourseInfo() {
		if (courseInfo != null && courseInfo.eIsProxy()) {
			InternalEObject oldCourseInfo = (InternalEObject)courseInfo;
			courseInfo = (CourseInfo)eResolveProxy(oldCourseInfo);
			if (courseInfo != oldCourseInfo) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoursePackage.COURSE__COURSE_INFO, oldCourseInfo, courseInfo));
			}
		}
		return courseInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseInfo basicGetCourseInfo() {
		return courseInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseInfo(CourseInfo newCourseInfo) {
		CourseInfo oldCourseInfo = courseInfo;
		courseInfo = newCourseInfo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE__COURSE_INFO, oldCourseInfo, courseInfo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department getDepartment() {
		if (department != null && department.eIsProxy()) {
			InternalEObject oldDepartment = (InternalEObject)department;
			department = (Department)eResolveProxy(oldDepartment);
			if (department != oldDepartment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoursePackage.COURSE__DEPARTMENT, oldDepartment, department));
			}
		}
		return department;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department basicGetDepartment() {
		return department;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDepartment(Department newDepartment) {
		Department oldDepartment = department;
		department = newDepartment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE__DEPARTMENT, oldDepartment, department));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseInstance> getCourseInstances() {
		if (courseInstances == null) {
			courseInstances = new EObjectResolvingEList<CourseInstance>(CourseInstance.class, this, CoursePackage.COURSE__COURSE_INSTANCES);
		}
		return courseInstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CreditReduction> getCreditReductions() {
		if (creditReductions == null) {
			creditReductions = new EObjectResolvingEList<CreditReduction>(CreditReduction.class, this, CoursePackage.COURSE__CREDIT_REDUCTIONS);
		}
		return creditReductions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursePackage.COURSE__CODE:
				return getCode();
			case CoursePackage.COURSE__NAME:
				return getName();
			case CoursePackage.COURSE__CREDITS:
				return getCredits();
			case CoursePackage.COURSE__LEVEL:
				return getLevel();
			case CoursePackage.COURSE__COURSE_INFO:
				if (resolve) return getCourseInfo();
				return basicGetCourseInfo();
			case CoursePackage.COURSE__DEPARTMENT:
				if (resolve) return getDepartment();
				return basicGetDepartment();
			case CoursePackage.COURSE__COURSE_INSTANCES:
				return getCourseInstances();
			case CoursePackage.COURSE__CREDIT_REDUCTIONS:
				return getCreditReductions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursePackage.COURSE__CODE:
				setCode((String)newValue);
				return;
			case CoursePackage.COURSE__NAME:
				setName((String)newValue);
				return;
			case CoursePackage.COURSE__CREDITS:
				setCredits((Double)newValue);
				return;
			case CoursePackage.COURSE__LEVEL:
				setLevel((String)newValue);
				return;
			case CoursePackage.COURSE__COURSE_INFO:
				setCourseInfo((CourseInfo)newValue);
				return;
			case CoursePackage.COURSE__DEPARTMENT:
				setDepartment((Department)newValue);
				return;
			case CoursePackage.COURSE__COURSE_INSTANCES:
				getCourseInstances().clear();
				getCourseInstances().addAll((Collection<? extends CourseInstance>)newValue);
				return;
			case CoursePackage.COURSE__CREDIT_REDUCTIONS:
				getCreditReductions().clear();
				getCreditReductions().addAll((Collection<? extends CreditReduction>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursePackage.COURSE__CODE:
				setCode(CODE_EDEFAULT);
				return;
			case CoursePackage.COURSE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CoursePackage.COURSE__CREDITS:
				setCredits(CREDITS_EDEFAULT);
				return;
			case CoursePackage.COURSE__LEVEL:
				setLevel(LEVEL_EDEFAULT);
				return;
			case CoursePackage.COURSE__COURSE_INFO:
				setCourseInfo((CourseInfo)null);
				return;
			case CoursePackage.COURSE__DEPARTMENT:
				setDepartment((Department)null);
				return;
			case CoursePackage.COURSE__COURSE_INSTANCES:
				getCourseInstances().clear();
				return;
			case CoursePackage.COURSE__CREDIT_REDUCTIONS:
				getCreditReductions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursePackage.COURSE__CODE:
				return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
			case CoursePackage.COURSE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CoursePackage.COURSE__CREDITS:
				return credits != CREDITS_EDEFAULT;
			case CoursePackage.COURSE__LEVEL:
				return LEVEL_EDEFAULT == null ? level != null : !LEVEL_EDEFAULT.equals(level);
			case CoursePackage.COURSE__COURSE_INFO:
				return courseInfo != null;
			case CoursePackage.COURSE__DEPARTMENT:
				return department != null;
			case CoursePackage.COURSE__COURSE_INSTANCES:
				return courseInstances != null && !courseInstances.isEmpty();
			case CoursePackage.COURSE__CREDIT_REDUCTIONS:
				return creditReductions != null && !creditReductions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (code: ");
		result.append(code);
		result.append(", name: ");
		result.append(name);
		result.append(", credits: ");
		result.append(credits);
		result.append(", level: ");
		result.append(level);
		result.append(')');
		return result.toString();
	}

} //CourseImpl
