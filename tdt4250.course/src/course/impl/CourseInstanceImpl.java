/**
 */
package course.impl;

import course.CourseInstance;
import course.CoursePackage;
import course.CourseWork;
import course.EvaluationForm;
import course.Semester;
import course.Staff;
import course.Timetable;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link course.impl.CourseInstanceImpl#getSemester <em>Semester</em>}</li>
 *   <li>{@link course.impl.CourseInstanceImpl#getYear <em>Year</em>}</li>
 *   <li>{@link course.impl.CourseInstanceImpl#getLanguage <em>Language</em>}</li>
 *   <li>{@link course.impl.CourseInstanceImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link course.impl.CourseInstanceImpl#getTimeTables <em>Time Tables</em>}</li>
 *   <li>{@link course.impl.CourseInstanceImpl#getEvaluationForms <em>Evaluation Forms</em>}</li>
 *   <li>{@link course.impl.CourseInstanceImpl#getCourseWork <em>Course Work</em>}</li>
 *   <li>{@link course.impl.CourseInstanceImpl#getStaff <em>Staff</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseInstanceImpl extends MinimalEObjectImpl.Container implements CourseInstance {
	/**
	 * The default value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected static final Semester SEMESTER_EDEFAULT = Semester.SPRING;

	/**
	 * The cached value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected Semester semester = SEMESTER_EDEFAULT;

	/**
	 * The default value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected static final int YEAR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected int year = YEAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getLanguage() <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguage()
	 * @generated
	 * @ordered
	 */
	protected static final String LANGUAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLanguage() <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguage()
	 * @generated
	 * @ordered
	 */
	protected String language = LANGUAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected String location = LOCATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTimeTables() <em>Time Tables</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeTables()
	 * @generated
	 * @ordered
	 */
	protected EList<Timetable> timeTables;

	/**
	 * The cached value of the '{@link #getEvaluationForms() <em>Evaluation Forms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvaluationForms()
	 * @generated
	 * @ordered
	 */
	protected EList<EvaluationForm> evaluationForms;

	/**
	 * The cached value of the '{@link #getCourseWork() <em>Course Work</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseWork()
	 * @generated
	 * @ordered
	 */
	protected CourseWork courseWork;

	/**
	 * The cached value of the '{@link #getStaff() <em>Staff</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStaff()
	 * @generated
	 * @ordered
	 */
	protected Staff staff;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePackage.Literals.COURSE_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Semester getSemester() {
		return semester;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSemester(Semester newSemester) {
		Semester oldSemester = semester;
		semester = newSemester == null ? SEMESTER_EDEFAULT : newSemester;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INSTANCE__SEMESTER, oldSemester, semester));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getYear() {
		return year;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setYear(int newYear) {
		int oldYear = year;
		year = newYear;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INSTANCE__YEAR, oldYear, year));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLanguage(String newLanguage) {
		String oldLanguage = language;
		language = newLanguage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INSTANCE__LANGUAGE, oldLanguage, language));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(String newLocation) {
		String oldLocation = location;
		location = newLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INSTANCE__LOCATION, oldLocation, location));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Timetable> getTimeTables() {
		if (timeTables == null) {
			timeTables = new EObjectResolvingEList<Timetable>(Timetable.class, this, CoursePackage.COURSE_INSTANCE__TIME_TABLES);
		}
		return timeTables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EvaluationForm> getEvaluationForms() {
		if (evaluationForms == null) {
			evaluationForms = new EObjectResolvingEList<EvaluationForm>(EvaluationForm.class, this, CoursePackage.COURSE_INSTANCE__EVALUATION_FORMS);
		}
		return evaluationForms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseWork getCourseWork() {
		if (courseWork != null && courseWork.eIsProxy()) {
			InternalEObject oldCourseWork = (InternalEObject)courseWork;
			courseWork = (CourseWork)eResolveProxy(oldCourseWork);
			if (courseWork != oldCourseWork) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoursePackage.COURSE_INSTANCE__COURSE_WORK, oldCourseWork, courseWork));
			}
		}
		return courseWork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseWork basicGetCourseWork() {
		return courseWork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseWork(CourseWork newCourseWork) {
		CourseWork oldCourseWork = courseWork;
		courseWork = newCourseWork;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INSTANCE__COURSE_WORK, oldCourseWork, courseWork));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Staff getStaff() {
		if (staff != null && staff.eIsProxy()) {
			InternalEObject oldStaff = (InternalEObject)staff;
			staff = (Staff)eResolveProxy(oldStaff);
			if (staff != oldStaff) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoursePackage.COURSE_INSTANCE__STAFF, oldStaff, staff));
			}
		}
		return staff;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Staff basicGetStaff() {
		return staff;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStaff(Staff newStaff) {
		Staff oldStaff = staff;
		staff = newStaff;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INSTANCE__STAFF, oldStaff, staff));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursePackage.COURSE_INSTANCE__SEMESTER:
				return getSemester();
			case CoursePackage.COURSE_INSTANCE__YEAR:
				return getYear();
			case CoursePackage.COURSE_INSTANCE__LANGUAGE:
				return getLanguage();
			case CoursePackage.COURSE_INSTANCE__LOCATION:
				return getLocation();
			case CoursePackage.COURSE_INSTANCE__TIME_TABLES:
				return getTimeTables();
			case CoursePackage.COURSE_INSTANCE__EVALUATION_FORMS:
				return getEvaluationForms();
			case CoursePackage.COURSE_INSTANCE__COURSE_WORK:
				if (resolve) return getCourseWork();
				return basicGetCourseWork();
			case CoursePackage.COURSE_INSTANCE__STAFF:
				if (resolve) return getStaff();
				return basicGetStaff();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursePackage.COURSE_INSTANCE__SEMESTER:
				setSemester((Semester)newValue);
				return;
			case CoursePackage.COURSE_INSTANCE__YEAR:
				setYear((Integer)newValue);
				return;
			case CoursePackage.COURSE_INSTANCE__LANGUAGE:
				setLanguage((String)newValue);
				return;
			case CoursePackage.COURSE_INSTANCE__LOCATION:
				setLocation((String)newValue);
				return;
			case CoursePackage.COURSE_INSTANCE__TIME_TABLES:
				getTimeTables().clear();
				getTimeTables().addAll((Collection<? extends Timetable>)newValue);
				return;
			case CoursePackage.COURSE_INSTANCE__EVALUATION_FORMS:
				getEvaluationForms().clear();
				getEvaluationForms().addAll((Collection<? extends EvaluationForm>)newValue);
				return;
			case CoursePackage.COURSE_INSTANCE__COURSE_WORK:
				setCourseWork((CourseWork)newValue);
				return;
			case CoursePackage.COURSE_INSTANCE__STAFF:
				setStaff((Staff)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursePackage.COURSE_INSTANCE__SEMESTER:
				setSemester(SEMESTER_EDEFAULT);
				return;
			case CoursePackage.COURSE_INSTANCE__YEAR:
				setYear(YEAR_EDEFAULT);
				return;
			case CoursePackage.COURSE_INSTANCE__LANGUAGE:
				setLanguage(LANGUAGE_EDEFAULT);
				return;
			case CoursePackage.COURSE_INSTANCE__LOCATION:
				setLocation(LOCATION_EDEFAULT);
				return;
			case CoursePackage.COURSE_INSTANCE__TIME_TABLES:
				getTimeTables().clear();
				return;
			case CoursePackage.COURSE_INSTANCE__EVALUATION_FORMS:
				getEvaluationForms().clear();
				return;
			case CoursePackage.COURSE_INSTANCE__COURSE_WORK:
				setCourseWork((CourseWork)null);
				return;
			case CoursePackage.COURSE_INSTANCE__STAFF:
				setStaff((Staff)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursePackage.COURSE_INSTANCE__SEMESTER:
				return semester != SEMESTER_EDEFAULT;
			case CoursePackage.COURSE_INSTANCE__YEAR:
				return year != YEAR_EDEFAULT;
			case CoursePackage.COURSE_INSTANCE__LANGUAGE:
				return LANGUAGE_EDEFAULT == null ? language != null : !LANGUAGE_EDEFAULT.equals(language);
			case CoursePackage.COURSE_INSTANCE__LOCATION:
				return LOCATION_EDEFAULT == null ? location != null : !LOCATION_EDEFAULT.equals(location);
			case CoursePackage.COURSE_INSTANCE__TIME_TABLES:
				return timeTables != null && !timeTables.isEmpty();
			case CoursePackage.COURSE_INSTANCE__EVALUATION_FORMS:
				return evaluationForms != null && !evaluationForms.isEmpty();
			case CoursePackage.COURSE_INSTANCE__COURSE_WORK:
				return courseWork != null;
			case CoursePackage.COURSE_INSTANCE__STAFF:
				return staff != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (semester: ");
		result.append(semester);
		result.append(", year: ");
		result.append(year);
		result.append(", language: ");
		result.append(language);
		result.append(", location: ");
		result.append(location);
		result.append(')');
		return result.toString();
	}

} //CourseInstanceImpl
