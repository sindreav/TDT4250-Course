/**
 */
package course;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluation Form</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link course.EvaluationForm#getType <em>Type</em>}</li>
 *   <li>{@link course.EvaluationForm#getWeight <em>Weight</em>}</li>
 *   <li>{@link course.EvaluationForm#getDuration <em>Duration</em>}</li>
 *   <li>{@link course.EvaluationForm#getExaminationAid <em>Examination Aid</em>}</li>
 * </ul>
 *
 * @see course.CoursePackage#getEvaluationForm()
 * @model
 * @generated
 */
public interface EvaluationForm extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link course.EvaluationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see course.EvaluationType
	 * @see #setType(EvaluationType)
	 * @see course.CoursePackage#getEvaluationForm_Type()
	 * @model
	 * @generated
	 */
	EvaluationType getType();

	/**
	 * Sets the value of the '{@link course.EvaluationForm#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see course.EvaluationType
	 * @see #getType()
	 * @generated
	 */
	void setType(EvaluationType value);

	/**
	 * Returns the value of the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Weight</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Weight</em>' attribute.
	 * @see #setWeight(double)
	 * @see course.CoursePackage#getEvaluationForm_Weight()
	 * @model
	 * @generated
	 */
	double getWeight();

	/**
	 * Sets the value of the '{@link course.EvaluationForm#getWeight <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Weight</em>' attribute.
	 * @see #getWeight()
	 * @generated
	 */
	void setWeight(double value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(double)
	 * @see course.CoursePackage#getEvaluationForm_Duration()
	 * @model
	 * @generated
	 */
	double getDuration();

	/**
	 * Sets the value of the '{@link course.EvaluationForm#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(double value);

	/**
	 * Returns the value of the '<em><b>Examination Aid</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Examination Aid</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Examination Aid</em>' reference.
	 * @see #setExaminationAid(ExaminationAid)
	 * @see course.CoursePackage#getEvaluationForm_ExaminationAid()
	 * @model
	 * @generated
	 */
	ExaminationAid getExaminationAid();

	/**
	 * Sets the value of the '{@link course.EvaluationForm#getExaminationAid <em>Examination Aid</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Examination Aid</em>' reference.
	 * @see #getExaminationAid()
	 * @generated
	 */
	void setExaminationAid(ExaminationAid value);

} // EvaluationForm
