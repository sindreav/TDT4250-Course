/**
 */
package course;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link course.CourseInstance#getSemester <em>Semester</em>}</li>
 *   <li>{@link course.CourseInstance#getYear <em>Year</em>}</li>
 *   <li>{@link course.CourseInstance#getLanguage <em>Language</em>}</li>
 *   <li>{@link course.CourseInstance#getLocation <em>Location</em>}</li>
 *   <li>{@link course.CourseInstance#getTimeTables <em>Time Tables</em>}</li>
 *   <li>{@link course.CourseInstance#getEvaluationForms <em>Evaluation Forms</em>}</li>
 *   <li>{@link course.CourseInstance#getCourseWork <em>Course Work</em>}</li>
 *   <li>{@link course.CourseInstance#getStaff <em>Staff</em>}</li>
 * </ul>
 *
 * @see course.CoursePackage#getCourseInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='evaluationFormSummedEqualsOne workAndTimetableMatchingInHours'"
 * @generated
 */
public interface CourseInstance extends EObject {
	/**
	 * Returns the value of the '<em><b>Semester</b></em>' attribute.
	 * The literals are from the enumeration {@link course.Semester}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Semester</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester</em>' attribute.
	 * @see course.Semester
	 * @see #setSemester(Semester)
	 * @see course.CoursePackage#getCourseInstance_Semester()
	 * @model
	 * @generated
	 */
	Semester getSemester();

	/**
	 * Sets the value of the '{@link course.CourseInstance#getSemester <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester</em>' attribute.
	 * @see course.Semester
	 * @see #getSemester()
	 * @generated
	 */
	void setSemester(Semester value);

	/**
	 * Returns the value of the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Year</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Year</em>' attribute.
	 * @see #setYear(int)
	 * @see course.CoursePackage#getCourseInstance_Year()
	 * @model
	 * @generated
	 */
	int getYear();

	/**
	 * Sets the value of the '{@link course.CourseInstance#getYear <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Year</em>' attribute.
	 * @see #getYear()
	 * @generated
	 */
	void setYear(int value);

	/**
	 * Returns the value of the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Language</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Language</em>' attribute.
	 * @see #setLanguage(String)
	 * @see course.CoursePackage#getCourseInstance_Language()
	 * @model
	 * @generated
	 */
	String getLanguage();

	/**
	 * Sets the value of the '{@link course.CourseInstance#getLanguage <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Language</em>' attribute.
	 * @see #getLanguage()
	 * @generated
	 */
	void setLanguage(String value);

	/**
	 * Returns the value of the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' attribute.
	 * @see #setLocation(String)
	 * @see course.CoursePackage#getCourseInstance_Location()
	 * @model
	 * @generated
	 */
	String getLocation();

	/**
	 * Sets the value of the '{@link course.CourseInstance#getLocation <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' attribute.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(String value);

	/**
	 * Returns the value of the '<em><b>Time Tables</b></em>' reference list.
	 * The list contents are of type {@link course.Timetable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Tables</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Tables</em>' reference list.
	 * @see course.CoursePackage#getCourseInstance_TimeTables()
	 * @model
	 * @generated
	 */
	EList<Timetable> getTimeTables();

	/**
	 * Returns the value of the '<em><b>Evaluation Forms</b></em>' reference list.
	 * The list contents are of type {@link course.EvaluationForm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluation Forms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluation Forms</em>' reference list.
	 * @see course.CoursePackage#getCourseInstance_EvaluationForms()
	 * @model
	 * @generated
	 */
	EList<EvaluationForm> getEvaluationForms();

	/**
	 * Returns the value of the '<em><b>Course Work</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Work</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Work</em>' reference.
	 * @see #setCourseWork(CourseWork)
	 * @see course.CoursePackage#getCourseInstance_CourseWork()
	 * @model
	 * @generated
	 */
	CourseWork getCourseWork();

	/**
	 * Sets the value of the '{@link course.CourseInstance#getCourseWork <em>Course Work</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Work</em>' reference.
	 * @see #getCourseWork()
	 * @generated
	 */
	void setCourseWork(CourseWork value);

	/**
	 * Returns the value of the '<em><b>Staff</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Staff</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Staff</em>' reference.
	 * @see #setStaff(Staff)
	 * @see course.CoursePackage#getCourseInstance_Staff()
	 * @model
	 * @generated
	 */
	Staff getStaff();

	/**
	 * Sets the value of the '{@link course.CourseInstance#getStaff <em>Staff</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Staff</em>' reference.
	 * @see #getStaff()
	 * @generated
	 */
	void setStaff(Staff value);

} // CourseInstance
