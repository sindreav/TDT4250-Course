<h3>Prerequisites</h3>
Libraries:
<ul>
	<li>org.eclipse.emf.common_2.15.0</li>
	<li>org.eclipse.emf.ecore_2.15.0</li>
</ul>
Referenced projects:
<ul>
	<li>tdt4250.course</li>
</ul>


<h3>How to use</h3>
The webpage builder transforms a course model instance into a webpage ala NTNUs own course page.<br>
You can run the WebpageBuilder.java with 2, 1 or none arguments:<br>
(argument 1) -> URI to a course model<br>
(argument 2) -> Output path for the webpage. (DO NOT use '/' on the end of this path)<br>

By running the WebpageBuilder without any arguments a preview webpage is made inside the Output folder within the project.<br>
<br>
!!DO NOT DELETE THE OUTPUT FOLDER!!
<br>
You can execute the program within Eclipse and provide arguments through the Run Configurations.<br>
<br>
Example on arguments:<br>
"C:\Users\user\Documents\My Courses\TDT4100.course" "C:\Users\user\Documents\My Websites"
